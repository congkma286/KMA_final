const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;
const { firestore } = require("./src/config/firebase/index");
const { doc, getDocs, updateDoc } = require("firebase/firestore");
const cors = require("cors");
app.use(cors());
//connect db
const db = require("./src/config/db/index");
db.connect();

//get json from body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan("combined"));

app.get("/", (req, res) => {
  res.json({ mess: "Hello" });
});

// app.use("/payment", require("./src/controllers/payment"));
app.use("/auth", require("./src/controllers/auth"));

app.use("/chat", require("./src/controllers/chat"));

app.use("/shop", require("./src/controllers/shop"));

app.use("/sync", require("./src/controllers/sync"));

app.use("/forgot", require("./src/controllers/forgot"));

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
