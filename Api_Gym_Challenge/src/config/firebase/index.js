const { initializeApp } = require("firebase/app");
const { getAnalytics } = require("firebase/analytics");
const {
  getFirestore,
  Timestamp,
  FieldValue,
} = require("firebase/firestore");

const firebaseConfig = {
  apiKey: "AIzaSyDua8f9Ct_BUz9SOCqurJr8XHThS003Ntk",
  authDomain: "gym-challenge-8bc6b.firebaseapp.com",
  projectId: "gym-challenge-8bc6b",
  storageBucket: "gym-challenge-8bc6b.appspot.com",
  messagingSenderId: "826690038410",
  appId: "1:826690038410:web:f8e0f3eb0c71f134041b73",
  measurementId: "G-R96DC97Z2Z"
};

const app = initializeApp(firebaseConfig);
const firestore = getFirestore(app);

module.exports = { firestore };
