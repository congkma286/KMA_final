const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema(
  {
    name: String,
    age: String,
    phone: String,
    role: String,
    userName: { type: String, required: true },
    password: { type: String, required: true, min: 6 },
    token: String,
    weight: Number,
    height: Number,
    gender: String,
    email: String,
    isActice: Boolean,
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
