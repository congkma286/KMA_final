const mongoose = require("mongoose");
const { Schema } = mongoose;

const chatSchema = new Schema(
  {
    idUser: String,
    userId: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Chat", chatSchema);
