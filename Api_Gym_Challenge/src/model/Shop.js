const mongoose = require("mongoose");
const { Schema } = mongoose;

const shopSchema = new Schema(
  {
    name: String,
    price: Number,
    img: String,
    link: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Shop", shopSchema);
