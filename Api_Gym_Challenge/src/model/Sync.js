const mongoose = require("mongoose");
const { Schema } = mongoose;

const syncSchema = new Schema(
  {
    userId: String,
    data: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Sync", syncSchema);
