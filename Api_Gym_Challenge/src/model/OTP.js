const mongoose = require("mongoose");
const { Schema } = mongoose;

const optSchema = new Schema(
  {
    idUser: String,
    otp: String,
    time: Number,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Otp", optSchema);
