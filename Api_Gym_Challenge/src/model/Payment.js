const mongoose = require("mongoose");
const { Schema } = mongoose;

const paymentSchema = new Schema(
  {
    idUser: String,
    money: Number,
    otp: String,
    timeOtp: String,
    moneyProcess: Number,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Payment", paymentSchema);
