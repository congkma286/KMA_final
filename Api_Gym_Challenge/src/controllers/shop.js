const express = require("express");
const app = express();
const users = require("../model/User");
const shops = require("../model/Shop");

app.get("/", (req, res) => {
  res.json("shop");
});

app.get("/product", (req, res) => {
  try {
    shops.find({}, async function (err, docs) {
      if (!err) {
        res.json({
          status: 1,
          message: "Thành công",
          data: docs,
        });
      } else res.status(400).json({ erore: "Error" });
    });
  } catch (error) {}
});

app.post("/add", (req, res) => {
  const shop = new shops(req.body);
  console.log(req.body);
  shop
    .save()
    .then(() => {
      res.json({
        status: 1,
        message: "Thêm thành công",
      });
    })
    .catch((error) => {
      res.json({
        status: 0,
        message: "Thêm thất bại",
        error: error,
      });
    });
});

app.post("/delete", (req, res) => {
  const id = req.body._id;

  shops
    .find({ _id: id })
    .remove()
    .then(() => {
      res.json({
        status: 1,
        message: "Xoá thành công",
      });
    })
    .catch((error) => {
      res.json({
        status: 0,
        message: "Xoá thất bại",
        error: error,
      });
    });
});

app.post("/update", (req, res) => {
  const { _id, name, price, img, link } = req.body;

  shops.updateOne(
    { _id: _id },
    { name, price, img, link },
    { new: true },
    function (err, _) {
      if (!err) {
        res.json({
          status: 1,
          message: "Cập nhật thành công",
        });
      } else {
        res.json({
          status: 0,
          message: "Cập nhật thất bại",
          data: {},
        });
      }
    }
  );
});

module.exports = app;
