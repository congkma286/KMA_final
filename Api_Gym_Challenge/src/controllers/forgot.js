const express = require("express");
const app = express();
const jwt = require("jsonwebtoken");
const {
  GOOGLE_MAILER_CLIENT_ID,
  GOOGLE_MAILER_CLIENT_SECRET,
  GOOGLE_MAILER_REFRESH_TOKEN,
  ADMIN_EMAIL_ADDRESS,
} = require("../config/key/Oauth");
const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");

const myOAuth2Client = new OAuth2Client(
  GOOGLE_MAILER_CLIENT_ID,
  GOOGLE_MAILER_CLIENT_SECRET
);

myOAuth2Client.setCredentials({
  refresh_token: GOOGLE_MAILER_REFRESH_TOKEN,
});

async function sendMail(email) {
  try {
    const subject = "Khôi phục tài khoản";
    const content = "Thông tin tài khoản của bạn";
    if (!email || !subject || !content)
      throw new Error("Please provide email, subject and content!");
    const myAccessTokenObject = await myOAuth2Client.getAccessToken();
    const myAccessToken = myAccessTokenObject?.token;
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: ADMIN_EMAIL_ADDRESS,
        clientId: GOOGLE_MAILER_CLIENT_ID,
        clientSecret: GOOGLE_MAILER_CLIENT_SECRET,
        refresh_token: GOOGLE_MAILER_REFRESH_TOKEN,
        accessToken: myAccessToken,
      },
    });

    const mailOptions = {
      from: ADMIN_EMAIL_ADDRESS,
      to: email, // Gửi đến ai?
      subject: subject, // Tiêu đề email
      text: `<h3>${content}</h3>`, // Nội dung email
    };

    await transport.sendMail(mailOptions);
    console.log("Email sent successfully.");
  } catch (error) {
    console.log("error", error);
  }
}

app.get("/", (req, res) => {
  res.json("forgot");
});

app.get("/requireSendMail", async (req, res) => {
  await sendMail(req.query.email)
    .then(() => {
      res.json({
        status: 0,
        message: "Gửi thành công",
      });
    })
    .catch((err) => {
      res.json({
        status: 0,
        message: "Gửi thất bại",
      });
    });
});

module.exports = app;
