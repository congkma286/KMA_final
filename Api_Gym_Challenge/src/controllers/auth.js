const express = require("express");
const app = express();
const users = require("../model/User");
const otps = require("../model/OTP");
const payments = require("../model/Payment");
const jwt = require("jsonwebtoken");
const SHA256 = require("crypto-js/sha256");
const { checkToken } = require("../utils/checkToken");
const KEY_JWT = require("../config/key");
const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");
const { handleSendMail } = require("../utils/senMail");

const checkIsExtis = async (data) => {
  const item = await users.find({ userName: data.userName });
  const item2 = await users.find({ userName: data.userName });
  return item?.length > 0 ? true : false;
};

const checkIsExtis2 = async (data) => {
  const item = await users.find({ email: data.email });
  return item?.length > 0 ? true : false;
};

app.get("/", (req, res) => {
  users.find({}, function (err, docs) {
    if (!err) res.json(docs);
    else res.status(400).json({ erore: "Error" });
  });
});

app.post("/login", (req, res) => {
  try {
    const { userName, password } = req.body;
    const hashPass = SHA256(password).toString();
    users.find({ userName, password: hashPass }, function (err, docs) {
      if (docs?.length > 0) {
        const data = docs[0];
        if (data.isActice) {
          data.token = jwt.sign(
            {
              exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 7, // hạn 7 ngày
              name: data.name,
              age: data.age,
              role: data.role,
              phone: data.phone,
              userName: data.userName,
              _id: data._id.toString(),
              height: data.height,
              weight: data.weight,
              gender: data.gender,
            },
            KEY_JWT
          );
          users.updateOne(
            { _id: data._id },
            data,
            { new: true },
            function (err, _) {
              if (!err) {
                res.json({
                  status: 1,
                  data: {
                    token: data.token,
                  },
                });
              } else {
                res.json({
                  status: 0,
                  message: "Đăng nhập thất bại",
                  data: {},
                });
              }
            }
          );
        } else {
          res.json({
            status: 2,
            message: "Tài khoản chưa được kích hoạt",
            data: {
              email: data.email,
              idUser: data._id.toString(),
            },
          });
        }
      } else {
        res.json({
          status: 0,
          message: "Đăng nhập thất bại",
        });
      }
    });
  } catch (error) {
    res.json({
      status: 0,
      message: "Đăng nhập thất bại",
    });
  }
});

function generateOTP() {
  const digits = "0123456789";
  let otp = "";

  for (let i = 0; i < 6; i++) {
    const randomIndex = Math.floor(Math.random() * digits.length);
    otp += digits[randomIndex];
  }

  return otp;
}

app.post("/register", async (req, res) => {
  const isExtis = await checkIsExtis(req.body);
  if (isExtis) {
    res.json({
      status: 0,
      message: "Tài khoản đã tồn tại",
    });
  } else {
    const isExtis = await checkIsExtis2(req.body);
    if (isExtis) {
      res.json({
        status: 0,
        message: "Email đã tồn tại",
      });
    }
    const user = new users(req.body);
    user.token = jwt.sign(
      {
        exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 7, // hạn 7 ngày
        name: user.name,
        age: user.age,
        phone: user.phone,
        role: "user",
        userName: user.userName,
        _id: user._id.toString(),
        height: user.height,
        weight: user.weight,
        gender: user.gender,
      },
      KEY_JWT
    );
    user.role = "user";
    user.password = SHA256(user.password);
    user.isActice = false;
    const userSave = new users(user);

    const strinOTP = generateOTP();

    const otp = new otps({
      idUser: user._id.toString(),
      otp: strinOTP,
      time: new Date().getTime(),
    });

    handleSendMail(user.email, strinOTP);

    otp
      .save()
      .then()
      .catch((error) => {
        res.json({
          status: 0,
          message: "Thêm thất bại",
          error: error,
        });
      });

    userSave
      .save()
      .then(() => {
        res.json({
          status: 1,
          message: "Thêm thành công",
          userId: user._id.toString(),
        });
      })
      .catch((error) => {
        res.json({
          status: 0,
          message: "Thêm thất bại",
          error: error,
        });
      });
  }
});

app.post("/checkToken", (req, res) => {
  try {
    const token = req.body.token;
    const decoded = checkToken(token);
    if (decoded?.active) {
      res.json(decoded.data);
    } else {
      res.json({
        status: 0,
        message: "Token không hợp lệ",
      });
    }
  } catch (error) {
    res.json({
      status: 0,
      message: error,
    });
  }
});

app.put("/login", (req, res) => {
  const { userName, password } = req.body;
  const passwordHash = SHA256(password);
  users.find({ userName, password: passwordHash }, function (err, docs) {
    if (!err) res.json(docs);
    else res.status(400).json({ erore: "Error" });
  });
});

app.put("/checkOtp", (req, res) => {
  const { idUser, otp } = req.body;
  otps.find({ idUser }, function (err, docs) {
    if (!err) {
      if (docs?.length > 0) {
        const data = docs[0];
        if (new Date().getTime() - new Date(data.time).getTime() > 90000) {
          res.json({
            status: 0,
            message: "Không thành công",
          });
        }
        if (otp == data?.otp) {
          users.find({ _id: idUser }, function (err, docs2) {
            if (docs2.length > 0) {
              const data2 = docs2[0];
              data2.isActice = true;
              users.updateOne(
                { _id: idUser },
                data2,
                { new: true },
                function (err, _) {
                  if (!err) {
                    res.json({
                      status: 1,
                      message: "Thành công",
                      data: {
                        token: data.token,
                      },
                    });
                  } else {
                    res.json({
                      status: 0,
                      message: "Thất bại",
                      data: {},
                    });
                  }
                }
              );
            } else {
              res.json({
                status: 0,
                message: "Không thành công",
              });
            }
          });

          res.json({
            status: 1,
            message: "Thành công",
          });
        } else {
          res.json({
            status: 0,
            message: "Không thành công",
          });
        }
      }
    } else res.status(400).json({ erore: "Error" });
  });
});

app.put("/sendForgotAccount", async (req, res) => {
  const { email } = req.body;
  const strinOTP = generateOTP();
  let idUser = "";
  users.find({ email }, function (err, docs) {
    if (docs.length > 0) {
      idUser = docs[0]._id.toString();
    }
  });
  handleSendMail(email, strinOTP);
  otps.find({ idUser }, function (err, docs) {
    otps.updateOne(
      { idUser: idUser },
      {
        otp: strinOTP,
        time: new Date().getTime(),
      },
      { new: true },
      function (err, _) {
        res.json({
          status: 1,
          message: "Thành công",
          userId: idUser,
        });
      }
    );
  });
});

app.put("/checkOtpForgot", async (req, res) => {
  const { idUser, otp, password } = req.body;
  otps.find({ idUser }, function (err, docs) {
    if (!err) {
      if (docs?.length > 0) {
        const data = docs[0];
        if (new Date().getTime() - new Date(data.time).getTime() > 90000) {
          res.json({
            status: 0,
            message: "Không thành công",
          });
        }
        if (otp == data?.otp) {
          users.find({ _id: idUser }, function (err, docs2) {
            if (docs2.length > 0) {
              const data2 = docs2[0];
              data2.password = SHA256(password).toString();
              users.updateOne(
                { _id: idUser },
                data2,
                { new: true },
                function (err, _) {
                  if (!err) {
                    res.json({
                      status: 1,
                      message: "Thành công",
                      data: {
                        token: data.token,
                      },
                    });
                  } else {
                    res.json({
                      status: 0,
                      message: "Thất bại",
                      data: {},
                    });
                  }
                }
              );
            } else {
              res.json({
                status: 0,
                message: "Không thành công",
              });
            }
          });

          res.json({
            status: 1,
            message: "Thành công",
          });
        } else {
          res.json({
            status: 0,
            message: "Không thành công",
          });
        }
      }
    } else res.status(400).json({ erore: "Error" });
  });
});

app.put("/sendOtpAgain", async (req, res) => {
  const { idUser, email } = req.body;
  const strinOTP = generateOTP();
  console.log("strinOTP", strinOTP);
  handleSendMail(email, strinOTP);
  otps.find({ idUser }, function (err, docs) {
    otps.updateOne(
      { idUser: idUser },
      {
        otp: strinOTP,
        time: new Date().getTime(),
      },
      { new: true },
      function (err, _) {
        res.json({
          status: 1,
          message: "Thành công",
        });
      }
    );
  });
});

app.put("/changePass", (req, res) => {
  const { _id, password, newPassword } = req.body;
  const passwordHash = SHA256(password).toString();
  const newPasswordHash = SHA256(newPassword).toString();
  users.find({ _id: _id, password: passwordHash }, function (err, docs) {
    if (!err) {
      const data = docs[0];
      data.password = newPasswordHash;
      users.updateOne({ _id: _id }, data, { new: true }, function (err, _) {
        res.json({
          status: 1,
          message: "Thành công",
        });
      });
    } else res.status(400).json({ erore: "Error" });
  });
});

module.exports = app;
