const express = require("express");
const app = express();
const users = require("../model/User");
const payments = require("../model/Payment");
const jwt = require("jsonwebtoken");
const SHA256 = require("crypto-js/sha256");
const { checkToken } = require("../utils/checkToken");
const { firestore } = require("../config/firebase/index");
const { doc, updateDoc } = require("firebase/firestore");

const generateOTP = () => {
  var digits = "0123456789";
  let OTP = "";
  for (let i = 0; i < 4; i++) {
    OTP += digits[Math.floor(Math.random() * 10)];
  }
  return ("000000" + OTP).substr(OTP.length);
};

const updateFirebase = async (phone, otp) => {
  const time = new Date().getTime();
  await updateDoc(doc(firestore, "otp", phone), {
    otp: otp,
    time: time + 90000,
  }).then((docRef) => {
    console.log("A New Document Field has been added to an existing document");
  });
};

app.post("/create", (req, res) => {
  const { accessToken, count } = req.body;
  users.findOne({ accessToken }, function (err, docs) {
    const decoded = checkToken(docs?.accessToken);
    if (decoded?.active) {
      const phone = decoded.data.phone;
      const idUser = decoded.data._id.toString();
      const otp = generateOTP();
      payments.updateOne(
        { idUser },
        {
          otp: otp,
          timeOtp: new Date().getTime(),
          moneyProcess: count,
        },
        { new: true },
        function (err, _) {
          updateFirebase(phone, otp);
          res.json({
            status: 1,
            message: "Tạo giao dịch thành công",
            data: {
              phone: phone,
            },
          });
        }
      );
    } else {
      res.json({
        status: 403,
        message: "Token không hợp lệ",
      });
    }
  });
});

app.put("/checkOTP", (req, res) => {
  const { accessToken, otp, phone } = req.body;
  users.findOne({ accessToken }, function (err, docs) {
    const decoded = checkToken(docs?.accessToken);
    if (decoded?.active) {
      const idUser = decoded.data._id.toString();
      payments.findOne({ idUser, otp }, function (err, docs) {
        if (!docs) {
          res.json({
            status: 0,
            message: "OTP không hợp lệ",
          });
        } else {
          payments.updateOne(
            { idUser },
            {
              money: docs.money - docs.moneyProcess,
              timeOtp: 0,
              otp: "",
              moneyProcess: 0,
            },
            { new: true },
            function (err, _) {
              updateFirebase(phone, "");
              res.json({
                status: 1,
                message: "Giao dịch thành công",
                data: {
                  phone: phone,
                },
              });
            }
          );
        }
      });
    } else {
      res.json({
        status: 403,
        message: "Token không hợp lệ",
      });
    }
  });
});

module.exports = app;
