const express = require("express");
const app = express();
const users = require("../model/User");
const payments = require("../model/Payment");
const chats = require("../model/Chat");
const jwt = require("jsonwebtoken");
const SHA256 = require("crypto-js/sha256");
const { checkToken } = require("../utils/checkToken");
const KEY_JWT = require("../config/key");
const { firestore } = require("../config/firebase/index");
const {
  doc,
  updateDoc,
  collection,
  getDoc,
  getDocs,
  addDoc,
  setDoc,
} = require("firebase/firestore");
const openai = require("openai");
const { Configuration, OpenAIApi } = require("openai");
const OpenAICustom = require("../utils/openApiCustom");
const Speech = require("../utils/speech");
const Conversation = require("../utils/conversation");

const got = require("got");

// const OPENAI_KEY = "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";
const OPENAI_KEY = "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";

const configuration = new Configuration({
  apiKey: OPENAI_KEY,
  id: "congkma1231823129u3912u312h",
});

function makeid(length) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

app.get("/", (req, res) => {
  users.find({}, function (err, docs) {
    if (!err) res.json("docs");
    else res.status(400).json({ erore: "Error" });
  });
});

app.get("/listChat", async (req, res) => {
  try {
    chats.find({}, async function (err, docs) {
      if (!err) {
        users.find({}, function (err, data) {
          res.json({
            status: 1,
            message: "Thành công",
            data: data,
          });
        });
      } else res.status(400).json({ erore: "Error" });
    });
  } catch (error) {
    console.log("error", error);
    res.status(400).json({ erore: "Error" });
  }
});

app.post("/sendId", (req, res) => {
  try {
    const item = chats.find({ userId: req.body.userId });
    if (item?.length === undefined) {
      const chat = new chats(req.body);
      chat
        .save()
        .then(() => {
          res.json({
            status: 1,
            message: "Thêm thành công",
          });
        })
        .catch((error) => {
          res.json({
            status: 0,
            message: "Thêm thất bại",
            error: error,
          });
        });
    } else {
      res.json({
        status: 1,
        message: "",
      });
    }
  } catch (error) {
    res.status(400).json({ erore: "Error" });
  }
});

app.post("/gpttest", async (req, res) => {
  try {
    // const tokenDecode = await jwt.decode(req.headers.authorization);
    // const { _id } = tokenDecode;
    const _id = "sadnjka";
    const openai = new OpenAIApi(configuration);
    const completion = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: req.body.prompt,
      max_tokens: 2048,
      temperature: 0,
    });
    await addDoc(collection(firestore, _id + "chatbot"), {
      _id: makeid(36),
      text: completion.data.choices[0].text,
      createdAt: new Date().getTime(),
      user: {
        _id: 3,
        name: "Bot GPT",
      },
    });
    res.status(200).json({
      text: completion.data.choices[0].text,
    });
  } catch (error) {
    if (error.response) {
      console.log(error.response.status);
      console.log(error.response.data);
    }
    if (error.response?.status === 429) {
      res.status(400).json({
        erore: "Rate limit exceeded. Please wait and try again later.",
      });
    } else {
      console.error(error);
      res.status(400).json({ erore: error });
    }
  }
});

app.post("/gpt", async (req, res) => {
  try {
    const tokenDecode = await jwt.decode(req.headers.authorization);
    const { prompt } = req.body;
    const { _id } = tokenDecode;
    const id = _id + "chatbot";
    const token = OPENAI_KEY;
    if (token) {
      OpenAICustom.setToken(token);
    }

    const speech = new Speech();

    const chat = new Conversation(id);
    chat.setMessage("question", prompt);

    const resp = await OpenAICustom.completions({ prompt: chat.makePromt() });
    console.log("resp", resp);

    if (resp && resp.choices) {
      const answer = resp.choices[0].text;
      const voice = speech.makeVoice(answer);
      const voices = speech.makeVoices(answer);
      chat.setMessage("answer", answer);

      await addDoc(collection(firestore, _id + "chatbot"), {
        _id: makeid(36),
        text: chat?.lastMessage?.answer,
        createdAt: new Date().getTime(),
        user: {
          _id: 3,
          name: "Bot GPT",
        },
      });
      res.status(200).json({
        text: completion.data.choices[0].text,
      });

      return res.send({
        id: chat._id,
        message: chat.lastMessage,
        voice,
        voices,
        items: chat._data.items,
      });
    }
  } catch (error) {
    if (error.response) {
      console.log(error.response.status);
      console.log(error.response.data);
    }
    if (error.response?.status === 429) {
      res.status(400).json({
        erore: "Rate limit exceeded. Please wait and try again later.",
      });
    } else {
      console.error(error);
      res.status(400).json({ erore: "Error" });
    }
  }
});

module.exports = app;
