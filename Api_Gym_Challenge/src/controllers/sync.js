const express = require("express");
const app = express();
const jwt = require("jsonwebtoken");
const syncs = require("../model/Sync");

app.get("/", (req, res) => {
  res.json("sync");
});

app.get("/check", async (req, res) => {
  const tokenDecode = await jwt.decode(req.headers.authorization);
  const { _id } = tokenDecode;
  const item = await syncs.find({ userId: _id });
  res.json({
    status: 1,
    data: item,
    message: "Thành công",
  });
});

app.post("/update", async (req, res) => {
  const tokenDecode = await jwt.decode(req.headers.authorization);
  const { _id } = tokenDecode;
  const data = JSON.stringify(req.body.data);
  syncs.updateOne(
    { userId: _id },
    { data: data },
    { new: true },
    function (err, _) {
      if (!err) {
        res.json({
          status: 1,
          data: data,
          message: "Đồng bộ thành công",
        });
      } else {
        res.json({
          status: 0,
          message: "Đồng bộ thất bại",
          data: {},
        });
      }
    }
  );
});

app.post("/add", async (req, res) => {
  const tokenDecode = await jwt.decode(req.headers.authorization);
  const { _id } = tokenDecode;
  const data = JSON.stringify(req.body.data);
  const dataSync = new syncs();
  dataSync.userId = _id;
  dataSync.data = data;
  dataSync
    .save()
    .then(() => {
      res.json({
        status: 1,
        message: "Đồng bộ thành công",
      });
    })
    .catch(() => {
      res.json({
        status: 0,
        message: "Đồng bộ thất bại",
        data: {},
      });
    });
});

module.exports = app;
