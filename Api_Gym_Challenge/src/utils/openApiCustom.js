const axios = require("axios");

axios.defaults.baseURL = "https://api.openai.com/v1/";
const OPENAI_KEY = "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";
const OpenAICustom = {
  token: null,

  setToken: function (token) {
    this.token = token;
  },

  _request: async function (path, predata = {}) {
    try {
      const headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${OPENAI_KEY}`,
      };
      console.log(`Request`, predata);
      const res = await axios.post(path, predata, { headers });
      const { data } = res;
      return data;
    } catch (error) {
      console.log(`Error`, error.message);
      return error;
    }
  },

  completions: async function (data) {
    const __default = {
      model: "text-davinci-003",
      prompt: "Say this is a test",
      max_tokens: 1000, // max to 2000
      temperature: 0,
      top_p: 1,
      stop: ["\n"],
    };
    return await this._request("completions", { ...__default, ...data });
  },
};

module.exports = OpenAICustom;
