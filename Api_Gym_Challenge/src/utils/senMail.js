const {
  GOOGLE_MAILER_CLIENT_ID,
  GOOGLE_MAILER_CLIENT_SECRET,
  GOOGLE_MAILER_REFRESH_TOKEN,
  ADMIN_EMAIL_ADDRESS,
} = require("../config/key/Oauth");
const nodemailer = require("nodemailer");
const { OAuth2Client } = require("google-auth-library");

const handleSendMail = async (email, otp) => {
  const myOAuth2Client = new OAuth2Client(
    GOOGLE_MAILER_CLIENT_ID,
    GOOGLE_MAILER_CLIENT_SECRET
  );

  myOAuth2Client.setCredentials({
    refresh_token: GOOGLE_MAILER_REFRESH_TOKEN,
  });

  try {
    const subject = "Mã xác thực OTP";
    if (!email || !subject)
      throw new Error("Please provide email, subject and content!");
    const myAccessTokenObject = await myOAuth2Client.getAccessToken();
    const myAccessToken = myAccessTokenObject?.token;
    const transport = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: ADMIN_EMAIL_ADDRESS,
        clientId: GOOGLE_MAILER_CLIENT_ID,
        clientSecret: GOOGLE_MAILER_CLIENT_SECRET,
        refresh_token: GOOGLE_MAILER_REFRESH_TOKEN,
        accessToken: myAccessToken,
      },
    });

    const mailOptions = {
      from: ADMIN_EMAIL_ADDRESS,
      to: email?.trim(), // Gửi đến ai?
      subject: subject, // Tiêu đề email
      text: `Mã OTP kích hoạt tài khoản: ${otp}`, // Nội dung email
    };

    await transport.sendMail(mailOptions);
    console.log("Email sent successfully." + mailOptions.to);
    console.log("Email sent successfully." + mailOptions.subject);
    console.log("Email sent successfully." + mailOptions.text);
  } catch (error) {
    console.log("error", error);
  }
};

module.exports = { handleSendMail };
