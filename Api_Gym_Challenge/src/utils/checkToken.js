const jwt = require("jsonwebtoken");
const KEY_JWT = require("../config/key/index");

const checkToken = (token) => {
  try {
    const decoded = jwt.verify(token, KEY_JWT);
    if (decoded)
      return {
        active: true,
        data: decoded,
      };
    return {
      active: false,
    };
  } catch (error) {
    return {
      active: false,
    };
  }
};

module.exports = { checkToken };
