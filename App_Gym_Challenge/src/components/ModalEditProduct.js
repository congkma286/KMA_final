import {
  View,
  Text,
  Modal,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from 'react-native';
import React, {useImperativeHandle, useState, useRef} from 'react';
import Toast from 'react-native-easy-toast';
import {postEditProduct} from '../services/apiShop';

const ModalEditProduct = (props, ref) => {
  const {reLoad} = props;
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [img, setImg] = useState('');
  const [link, setLink] = useState('');
  const [_id, setId] = useState('');

  const refToast = useRef(null);

  const onSubmit = async () => {
    if (!name) {
      refToast.current.show('Yêu cầu nhập tên sản phẩm', 2000);
      return;
    }
    if (!price) {
      refToast.current.show('Yêu cầu nhập giá sản phẩm', 2000);
      return;
    }
    if (isNaN(price)) {
      console.log('first');
      refToast.current.show('Yêu cầu nhập giá sản phẩm bằng số', 2000);
      return;
    }
    if (!img) {
      refToast.current.show('Yêu cầu nhập đường dẫn anhe sản phẩm', 2000);
      return;
    }
    if (!link) {
      refToast.current.show('Yêu cầu nhập đường dẫn sản phẩm', 2000);
      return;
    }

    const res = await postEditProduct({_id, name, price, img, link});
    if (res.status === 1) {
      Alert.alert('', 'Sửa sản phẩm thành công', [
        {
          text: 'Đồng ý',
          onPress: () => {
            reLoad();
            onHide();
          },
        },
      ]);
    } else {
      Alert.alert('', 'Sửa sản phẩm không thành công', []);
    }
  };

  const onShow = ({name, price, img, link, _id}) => {
    setModalVisible(true);
    setName(name);
    setPrice(price.toString());
    setImg(img);
    setLink(link);
    setId(_id);
  };

  const onHide = () => {
    setModalVisible(false);
  };

  useImperativeHandle(ref, () => ({
    onShow,
    onHide,
  }));

  const Dot = () => {
    return <Text style={styles.txtDotRed}> *</Text>;
  };

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <TouchableWithoutFeedback onPress={onHide}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          style={styles.container}>
          <TouchableWithoutFeedback>
            <View style={styles.main}>
              <Text style={styles.title}>Sửa sản phẩm</Text>
              <Text style={styles.txtName}>
                Nhập tên sản phẩm
                <Dot />
              </Text>
              <TextInput
                style={styles.inputName}
                value={name}
                onChangeText={setName}
              />
              <Text style={styles.txtName}>
                Nhập giá sản phẩm
                <Dot />
              </Text>
              <TextInput
                style={styles.inputName}
                value={price}
                onChangeText={setPrice}
              />
              <Text style={styles.txtName}>
                Nhập đường dẫn ảnh sản phẩm
                <Dot />
              </Text>
              <TextInput
                style={styles.inputName}
                value={img}
                onChangeText={setImg}
              />
              <Text style={styles.txtName}>
                Nhập đường dẫn sản phẩm
                <Dot />
              </Text>
              <TextInput
                style={styles.inputName}
                value={link}
                onChangeText={setLink}
              />
              <TouchableWithoutFeedback onPress={onSubmit}>
                <View style={styles.btSubmit}>
                  <Text style={styles.txtBtSubmit}>Sửa sản phẩm</Text>
                </View>
              </TouchableWithoutFeedback>

              <Toast position="top" ref={refToast} />
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
  },
  main: {
    width: '90%',
    height: 500,
    alignSelf: 'center',
    backgroundColor: '#181c29',
    borderRadius: 10,
    paddingHorizontal: '5%',
  },
  title: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '500',
    color: '#5AC685',
    marginTop: 16,
  },
  txtName: {
    fontSize: 16,
    fontWeight: '500',
    color: '#5AC685',
    marginTop: 20,
  },
  inputName: {
    fontSize: 16,
    fontWeight: '500',
    color: '#fff',
    marginTop: 8,
    height: 40,
    width: '100%',
    alignSelf: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    paddingHorizontal: '5%',
  },
  txtDotRed: {
    color: '#fd0000',
  },
  btSubmit: {
    width: '60%',
    alignSelf: 'center',
    height: 50,
    backgroundColor: '#5AC685',
    marginTop: 24,
    justifyContent: 'center',
    borderRadius: 30,
  },
  txtBtSubmit: {
    alignSelf: 'center',
    fontSize: 18,
    color: '#fff',
    fontWeight: '700',
  },
});

export default React.forwardRef(ModalEditProduct);
