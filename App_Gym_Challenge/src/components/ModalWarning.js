import {
  View,
  Text,
  Modal,
  StyleSheet,
  ImageBackground,
  Dimensions,
} from 'react-native';
import React, {useImperativeHandle, useState} from 'react';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
var Sound = require('react-native-sound');

const {width} = Dimensions.get('window');

const whoosh = new Sound('whoosh.mp3', Sound.MAIN_BUNDLE, error => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
  // loaded successfully
  console.log(
    'duration in seconds: ' +
      whoosh.getDuration() +
      'number of channels: ' +
      whoosh.getNumberOfChannels(),
  );

  // Play the sound with an onEnd callback
});

const ModalWarning = (props, ref) => {
  const {onPress} = props;
  const [modalVisible, setModalVisible] = useState(false);

  const onShow = () => {
    setModalVisible(true);
    whoosh.play(success => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
    });
  };

  const onHide = () => {
    setModalVisible(false);
    whoosh.stop();
  };

  useImperativeHandle(
    ref,
    () => ({
      onShow: onShow,
      onHide: onHide,
    }),
    [],
  );

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <View style={styles.centeredView}>
        <ImageBackground
          style={styles.modalView}
          imageStyle={{borderRadius: 10, opacity: 0.7}}
          source={require('../assets/images/image_tired.png')}>
          <View>
            <Text style={styles.txtWarning}>Cảnh báo</Text>
            <Text style={styles.txt}>Hệ thống cho biết bạn đang treo máy</Text>
            <TouchableWithoutFeedback onPress={onPress}>
              <View style={styles.btConti}>
                <Text style={styles.txtConti}>Tiếp tục</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </ImageBackground>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'center',
  },
  modalView: {
    alignSelf: 'center',
    width: width * 0.9,
    height: width * 0.9,
    borderRadius: 10,
    backgroundColor: '#000',
    justifyContent: 'center',
  },
  txtWarning: {
    alignSelf: 'center',
    fontSize: 26,
    fontWeight: '700',
    color: '#01D154',
  },
  txt: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '600',
    color: '#fff',
    marginTop: 20,
    width: '90%',
    textAlign: 'center',
  },
  btConti: {
    width: '50%',
    height: 40,
    borderRadius: 10,
    backgroundColor: '#01D154',
    alignSelf: 'center',
    marginTop: 40,
    justifyContent: 'center',
  },
  txtConti: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
    textAlign: 'center',
  },
});

export default React.forwardRef(ModalWarning);
