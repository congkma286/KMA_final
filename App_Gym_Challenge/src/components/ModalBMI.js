import {View, Text, Modal, StyleSheet, FlatList} from 'react-native';
import React, {useImperativeHandle, useState} from 'react';
import {standardMale} from '../constants/standardMale';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {standardFemale} from '../constants/standardFemale';
import BMI from './BMIMale';

const ModalBMI = (props, ref) => {
  const [modalVisible, setModalVisible] = useState(false);

  const [gender, setGender] = useState('');
  const [height, setHeight] = useState(0);
  const [weight, setWeight] = useState(0);

  const onPressHide = () => {
    setModalVisible(false);
  };

  const onShow = ({gender, height, weight}) => {
    setModalVisible(true);
    setGender(gender);
    setHeight(height);
    setWeight(weight);
  };

  useImperativeHandle(
    ref,
    () => ({
      onShow,
    }),
    [],
  );

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.row}>
        <Text style={styles.txt}>
          {item?.weight}  {item?.height}{' '} cm
        </Text>
      </View>
    );
  };

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <View style={styles.contain}>
        <View style={styles.wallet}>
          <Text style={styles.txtTT2}>Bảng tỉ lệ cân đối tiêu chuẩn</Text>
          <FlatList
            data={standardFemale}
            renderItem={renderItem}
            numColumns={2}
            style={styles.fl}
            showsVerticalScrollIndicator={false}
          />
          <TouchableWithoutFeedback onPress={onPressHide}>
            <View style={styles.bt}>
              <Text style={styles.txttx123}>Xác nhận</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'center',
  },
  wallet: {
    width: '92%',
    height: '80%',
    alignSelf: 'center',
    backgroundColor: '#181c29',
    borderRadius: 10,
    paddingHorizontal: '2%',
  },
  txtTT: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: '#5AC685',
    marginTop: 20,
  },
  txtTT2: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: '#5AC685',
    marginTop: 10,
  },
  fl: {
    marginTop: 20,
  },
  row: {
    width: '50%',
    height: 40,
    borderWidth: 1,
    justifyContent: 'center',
    borderColor: '#fff',
  },
  txt: {
    color: '#fff',
    alignSelf: 'center',
  },
  bt: {
    width: '60%',
    height: 40,
    backgroundColor: '#5AC685',
    alignSelf: 'center',
    borderRadius: 40,
    justifyContent: 'center',
    marginBottom: 15,
    marginTop: 10,
  },
  txttx123: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '600',
    color: '#fff',
  },
});

export default React.forwardRef(ModalBMI);
