import BottomSheet from '@gorhom/bottom-sheet';
import React, {useImperativeHandle, useRef, useState} from 'react';

const ModalBottom = (props, ref) => {
  const {snapPoints = ['25%', '50%']} = props;
  const bottomSheetRef = useRef(null);

  const onShow = () => {
    bottomSheetRef.current.snapToIndex(0);
  };

  const onHide = () => {
    bottomSheetRef.current.onHide();
  };

  useImperativeHandle(ref, () => ({
    onShow: onShow,
    onHide: onHide,
  }));

  const onChange = a => {
    if (a === -1) {
      try {
        props?.callBackHide();
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <BottomSheet
      ref={bottomSheetRef}
      index={-1}
      snapPoints={snapPoints}
      onChange={onChange}
      enablePanDownToClose={true}>
      {props.children}
    </BottomSheet>
  );
};

export default React.forwardRef(ModalBottom);
