import {
  View,
  Text,
  Modal,
  StyleSheet,
  ImageBackground,
  SafeAreaView,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useState, useImperativeHandle} from 'react';
import {useNavigation} from '@react-navigation/native';
import {setDataByKey} from '../utils/strorage';
import {useDispatch, useSelector} from 'react-redux';
import {requestReload} from '../redux/dataSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import _ from 'lodash';
import moment from 'moment';
import convertTime from '../utils/convertTime';

const {height} = Dimensions.get('window');

const ModalComplete = (props, ref) => {
  const {data, parent} = props;
  console.log('props', props);
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation();
  const [timeExercising, setTimeExercising] = useState(0);
  console.log('timeExercising', timeExercising);
  let calo = 0;

  const {time} = useSelector(state => state.data);

  const dispatch = useDispatch();

  data?.map(i => {
    calo += i.calo;
  });

  const onShow = () => {
    setTimeExercising(Math.floor((new Date().getTime() - time) / 1000));
    setModalVisible(true);
  };

  const onHide = () => {};

  useImperativeHandle(
    ref,
    () => ({
      onShow: onShow,
      onHide: onHide,
    }),
    [],
  );

  const onShare = () => {};

  const onComplete = async () => {
    let itemChange = {...parent};
    itemChange.day = parent.day + 1;
    itemChange.progress = Number(Number(parent.progress) + 3.33).toFixed(2);
    setDataByKey(itemChange.id, itemChange);
    const timeDay = moment(new Date().getTime()).format('DD/MM/YYYY');
    let dataSave = {
      ...parent,
      timeDay: timeDay,
      calo: calo,
      timeExercising: timeExercising,
      progress: Number(Number(parent.progress) + 3.33).toFixed(2),
    };
    const dataHistory = await AsyncStorage.getItem('@history');
    if (dataHistory !== null) {
      const data = JSON.parse(dataHistory);
      data.push(dataSave);
      await AsyncStorage.setItem('@history', JSON.stringify(data));
    } else {
      await AsyncStorage.setItem('@history', JSON.stringify([dataSave]));
    }
    dispatch(requestReload());
    navigation.navigate('Home');
  };

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <ImageBackground
        source={require('../assets/images/image_backg_complete.png')}
        style={styles.centeredView}>
        <View style={styles.modalView}>
          <SafeAreaView />
          <Text style={styles.title}>TẬP TOÀN THÂN DỄ - NGÀY 1</Text>
        </View>
        <View style={styles.bottom}>
          <View style={styles.rowHeader}>
            <View style={styles.itemHeader}>
              <Text style={styles.title1}>Bài tập</Text>
              <Text style={styles.txtValue}>{data?.length}</Text>
            </View>
            <View style={styles.itemHeader}>
              <Text style={styles.title1}>Kcal</Text>
              <Text style={styles.txtValue}>{calo}</Text>
            </View>
            <View style={styles.itemHeader}>
              <Text style={styles.title1}>Thời lượng</Text>
              <Text style={styles.txtValue}>{convertTime(timeExercising)}</Text>
            </View>
          </View>
          <TouchableWithoutFeedback onPress={onShare}>
            <View style={styles.btShare}>
              {/* <Text style={styles.txtBt}>Chia sẻ với bạn bè</Text> */}
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onComplete}>
            <View style={styles.btComplete}>
              <Text style={styles.txtBt}>Hoàn thành</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </ImageBackground>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
  },
  modalView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  title: {
    marginLeft: '5%',
    fontSize: 22,
    fontWeight: '600',
    color: '#fff',
    marginTop: 50,
  },
  bottom: {
    position: 'absolute',
    alignSelf: 'center',
    width: '96%',
    marginTop: height - 240,
    height: 220,
    backgroundColor: '#303545',
    borderRadius: 20,
  },
  rowHeader: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  itemHeader: {
    width: '33%',
  },
  title1: {
    alignSelf: 'center',
    fontSize: 18,
    color: '#fff',
    fontWeight: '600',
  },
  txtValue: {
    alignSelf: 'center',
    fontSize: 18,
    color: '#5AC685',
    fontWeight: '600',
    marginTop: 8,
  },
  btShare: {
    alignSelf: 'center',
    width: '90%',
    height: 50,
    // backgroundColor: '#5AC685',
    marginTop: 20,
    borderRadius: 30,
    justifyContent: 'center',
  },
  btComplete: {
    alignSelf: 'center',
    width: '90%',
    height: 50,
    backgroundColor: '#454958',
    marginTop: 10,
    borderRadius: 30,
    justifyContent: 'center',
  },
  txtBt: {
    alignSelf: 'center',
    fontSize: 20,
    color: '#fff',
    fontWeight: '600',
  },
});

export default React.forwardRef(ModalComplete);
