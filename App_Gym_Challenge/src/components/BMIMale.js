// BMI = Cân nặng / (Chiều cao)2
// BMI <16: Bạn thuộc nhóm Gầy độ 3;
// 16=<BMI=<16.99: Bạn thuộc nhóm Gầy độ 2;
// 17=<BMI=<18.49: Bạn thuộc nhóm Gầy độ 1;
// 18.5 ---> 24.49 Bình thường;
// 25=<BMI=<29.99: Bạn thuộc nhóm Tiền béo phì;
// 30=<BMI=<34.99: Bạn thuộc nhóm Béo phì độ 1;
// 35=<BMI=<39.99: Bạn thuộc nhóm Béo phì độ 2;
// 40=<BMI: Bạn thuộc nhóm Béo phì độ 3.

const BMI = (weight, height) => {
  const BMI = weight / ((height / 100) * (height / 100));
  if (BMI < 16) {
    return 'Bạn thuộc nhóm Gầy độ 3';
  }
  if (BMI <= 16.99) {
    return 'Bạn thuộc nhóm Gầy độ 2';
  }
  if (BMI <= 18.49) {
    return 'Bạn thuộc nhóm Gầy độ 1';
  }
  if (BMI <= 24.49) {
    return 'Bạn thuộc nhóm cân đối';
  }
  if (BMI <= 29.99) {
    return 'Bạn thuộc nhóm Tiền béo phì';
  }
  if (BMI <= 34.99) {
    return 'Bạn thuộc nhóm Tiền béo phì độ 1';
  }
  if (BMI <= 39.99) {
    return 'Bạn thuộc nhóm Béo phì độ 2';
  }
  if (BMI >= 40) {
    return 'Bạn thuộc nhóm Béo phì độ 3';
  }
};

export default BMI;
