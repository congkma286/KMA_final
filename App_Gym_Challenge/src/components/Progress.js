import {View, Text, StyleSheet} from 'react-native';
import React from 'react';

const Progress = ({width, progress, top, height = 6, color = '#56CCF2'}) => {
  const getProgress = pro => {
    return pro + '%';
  };

  return (
    <View style={[styles.contain, {width, top, height}]}>
      <View
        style={[
          styles.progress,
          {width: getProgress(progress), height, backgroundColor: color},
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  contain: {
    borderRadius: 5,
    backgroundColor: '#c4c4c4',
    alignSelf: 'center',
  },
  progress: {
    borderRadius: 5,
  },
});

export default Progress;
