import {View, Text, Modal, StyleSheet} from 'react-native';
import React, {useState, useImperativeHandle, useEffect} from 'react';

const ModalCountDown = (props, ref) => {
  const {handleAfter = () => {}} = props;
  const [modalVisible, setModalVisible] = useState(false);
  const [countDown, setCountDown] = useState(0);

  const onShow = num => {
    setCountDown(num);
    setModalVisible(true);
  };

  const onHide = () => {
    setModalVisible(false);
  };

  useImperativeHandle(
    ref,
    () => ({
      onShow,
      onHide,
    }),
    [],
  );

  useEffect(() => {
    let funcCountDown = () => {};
    if (modalVisible) {
      funcCountDown = setInterval(() => {
        if (!countDown) {
          onHide();
          handleAfter();
        } else {
          setCountDown(countDown - 1);
        }
      }, 1000);
    }

    return () => {
      clearInterval(funcCountDown);
    };
  }, [modalVisible, countDown, handleAfter]);

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.txtNum}>
            {countDown ? countDown : 'Sẵn sàng!!!'}
          </Text>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
  },
  modalView: {
    alignSelf: 'center',
    width: '60%',
    height: 120,
    borderRadius: 10,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  txtNum: {
    alignSelf: 'center',
    fontSize: 40,
    fontWeight: '700',
    fontFamily: 'Nunito',
    color: '#5AC685',
  },
});

export default React.forwardRef(ModalCountDown);
