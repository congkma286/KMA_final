import {faMedal, faCircleInfo} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useImperativeHandle, useState} from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback as Tou,
  Dimensions,
} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {setDataByKey} from '../utils/strorage';
import {useDispatch} from 'react-redux';
import {requestReload} from '../redux/dataSlice';

const {width} = Dimensions.get('window');

const ModalLevel = (props, ref) => {
  const [visible, setVisible] = useState(false);
  const {data, loadAgain} = props;
  const dispatch = useDispatch();

  const [isVisibleModalInfo, setIsVisibleModalInfo] = useState(false);

  const onPressLevelItem = async ({level, title}) => {
    if (level === data?.level) {
      setVisible(false);
    } else {
      const dataHistoryType = await AsyncStorage.getItem(`@History${data?.id}`);
      if (dataHistoryType) {
        const dataHistoryTypeJson = JSON.parse(dataHistoryType);
        const dataShow = dataHistoryTypeJson?.filter(a => a?.level === level);
        if (dataShow?.length > 0) {
          loadAgain({data: dataShow[0], level: level});
          setDataByKey(data.id, dataShow[0]);
        } else {
          const dataCurrentSave = {
            day: 1,
            id: data?.id,
            level: level,
            progress: 0,
            title: data?.title,
          };
          loadAgain({data: dataCurrentSave, level: level});
          setDataByKey(data.id, dataCurrentSave);
        }

        //lưu lại
        const dataNeed = dataHistoryTypeJson?.filter(a => a?.level !== level);
        dataNeed.push(data);
        await AsyncStorage.setItem(
          `@History${data?.id}`,
          JSON.stringify(dataNeed),
        );
      } else {
        const dataCurrentSave = {
          day: 1,
          id: data?.id,
          level: level,
          progress: 0,
          title: data?.title,
        };
        loadAgain({data: dataCurrentSave, level: level});
        setDataByKey(data.id, dataCurrentSave);
        await AsyncStorage.setItem(
          `@History${data?.id}`,
          JSON.stringify([data]),
        );
      }
      dispatch(requestReload());
      setVisible(false);
    }
  };

  const onShow = () => {
    setVisible(true);
  };

  const onHide = () => {
    setVisible(false);
  };

  useImperativeHandle(
    ref,
    () => ({
      onShow,
      onHide,
    }),
    [],
  );

  const hideModalInfo = () => {
    setIsVisibleModalInfo(false);
  };

  const showModalInfo = () => {
    setIsVisibleModalInfo(true);
  };

  return (
    <Modal transparent visible={visible}>
      <Tou onPress={onHide}>
        <View style={styles.contain}>
          <Tou>
            <View style={styles.wallet}>
              <View style={styles.rowHeader}>
                <View style={styles.headerLeft} />
                <Text style={styles.title}>Mức độ tập luyện</Text>
                <TouchableWithoutFeedback onPress={showModalInfo}>
                  <FontAwesomeIcon
                    style={styles.headerRight}
                    color={'#fff'}
                    size={22}
                    icon={faCircleInfo}
                  />
                </TouchableWithoutFeedback>
              </View>
              <TouchableWithoutFeedback
                onPress={() => onPressLevelItem({level: 1, title: 'Dễ'})}>
                <View style={styles.rowRank}>
                  <FontAwesomeIcon color={'#E17540'} size={22} icon={faMedal} />
                  <Text style={styles.txtRank}>Dễ</Text>
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback
                onPress={() =>
                  onPressLevelItem({level: 2, title: 'Trung bình'})
                }>
                <View style={styles.rowRank}>
                  <FontAwesomeIcon color={'#C3C6CD'} size={22} icon={faMedal} />
                  <Text style={styles.txtRank}>Trung bình</Text>
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback
                onPress={() => onPressLevelItem({level: 3, title: 'Khó'})}>
                <View style={styles.rowRank}>
                  <FontAwesomeIcon color={'#EFD05C'} size={22} icon={faMedal} />
                  <Text style={styles.txtRank}>Khó</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </Tou>
        </View>
      </Tou>
      <Modal transparent visible={isVisibleModalInfo}>
        <View style={styles.containModal}>
          <TouchableWithoutFeedback>
            <View style={styles.viewInfo}>
              <View style={styles.rowRank}>
                <View style={styles.leftRowRank}>
                  <FontAwesomeIcon color={'#E17540'} size={22} icon={faMedal} />
                </View>
                <Text style={styles.descriptionRank}>
                  Kế hoạch tập luyện dễ dàng được thiết kế cho những người tập
                  thể dục 1 lần một tuần
                </Text>
              </View>
              <View style={styles.rowRank}>
                <View style={styles.leftRowRank}>
                  <FontAwesomeIcon color={'#C3C6CD'} size={22} icon={faMedal} />
                </View>
                <Text style={styles.descriptionRank}>
                  Kế hoạch tập luyện trung bình được thiết kế cho những người
                  tập từ 2-3 lần một tuần.
                </Text>
              </View>
              <View style={styles.rowRank}>
                <View style={styles.leftRowRank}>
                  <FontAwesomeIcon color={'#EFD05C'} size={22} icon={faMedal} />
                </View>
                <Text style={styles.descriptionRank}>
                  Kế hoạch tập luyện trung bình được thiết kế cho những người
                  tập từ 4-6 lần một tuần.
                </Text>
              </View>
              <TouchableWithoutFeedback onPress={hideModalInfo}>
                <Text style={styles.txtUnderstand}>ĐÃ HIỂU</Text>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </Modal>
    </Modal>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'center',
  },
  wallet: {
    width: '90%',
    height: 330,
    backgroundColor: '#181c29',
    alignSelf: 'center',
    borderRadius: 5,
  },
  title: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '500',
    color: '#fff',
    top: 20,
    marginBottom: 20,
  },
  rowRank: {
    flexDirection: 'row',
    height: 50,
    width: '100%',
    alignSelf: 'center',
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: '8%',
  },
  txtRank: {
    alignSelf: 'center',
    fontSize: 17,
    fontWeight: '600',
    marginLeft: 10,
    color: '#fff',
  },

  containModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
  },
  viewInfo: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#323746',
    borderRadius: 5,
    paddingBottom: 40,
    paddingVertical: 20,
  },
  rowRank: {
    flexDirection: 'row',
    height: 70,
    width: '100%',
    alignSelf: 'center',
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: '5%',
  },
  txtRank: {
    alignSelf: 'center',
    fontSize: 17,
    fontWeight: '600',
    marginLeft: 10,
    color: '#fff',
  },
  boundary: {
    width: '100%',
    height: 15,
  },
  leftRowRank: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  descriptionRank: {
    alignSelf: 'center',
    fontSize: 14,
    marginLeft: 5,
    color: '#fff',
    width: width * 0.9 - 60,
  },
  txtUnderstand: {
    alignSelf: 'center',
    marginTop: 30,
    fontSize: 22,
    fontWeight: '600',
    color: '#77C38A',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },

  rowHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '94%',
    alignSelf: 'center',
  },
  headerLeft: {
    width: 22,
    height: 22,
  },
  headerRight: {
    alignSelf: 'center',
    marginTop: 20,
    left: -5,
  },
});

export default React.forwardRef(ModalLevel);
