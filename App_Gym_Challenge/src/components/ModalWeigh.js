import {View, Text, Modal, StyleSheet} from 'react-native';
import React, {useImperativeHandle, useState, useRef} from 'react';
import TextAnimator from './TextAnimator';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {TextInput} from 'react-native';
import Toast from 'react-native-easy-toast';
import {setWeigh} from '../utils/strorage';

const ModalWeigh = (props, ref) => {
  const [visible, setVisible] = useState(false);

  const [gender, setGender] = useState('NAM');
  const [numHeight, setNumHeight] = useState();
  const [numWeight, setNumWeight] = useState();

  const refToast = useRef(null);

  const onSubmit = async () => {
    if (numHeight) {
      if(numHeight < 140){
        refToast.current.show('Chiều cao phải lớn hơn 140', 2000);
        return
      }
      if(numHeight > 200){
        refToast.current.show('Chiều cao phải nhỏ hơn 200', 2000);
        return
      }
      if (numWeight) {
        if(numWeight < 35){
          refToast.current.show('Cân nặng phải lớn hơn 35', 2000);
          return
        }
        if(numWeight > 90){
          refToast.current.show('Cân nặng phải nhỏ hơn 90', 2000);
          return
        }
        await setWeigh({gender: gender, weigh: numWeight, height: numHeight});
        onHide();
      } else {
        refToast.current.show('Hãy nhập cân nặng', 2000);
      }
    } else {
      refToast.current.show('Hãy nhập chiều cao', 2000);
    }
  };

  const onShow = () => {
    setVisible(true);
  };

  const onHide = () => {
    setVisible(false);
  };

  useImperativeHandle(
    ref,
    () => ({
      onShow,
    }),
    [],
  );

  return (
    <Modal visible={visible} transparent>
      <View style={styles.contain}>
        <View style={styles.wallet}>
          <TextAnimator
            content="Để mang lại trải nghiệm tốt và đề xuất bài tập thích hợp bạn hay điền thông tin sau!"
            textStyle={styles.textStyle}
            style={styles.containerStyle}
            duration={1000}
            onFinish={() => {}}
          />
          <View style={styles.row}>
            <Text style={styles.title}>Giới tính</Text>
            <View style={styles.boxGender}>
              <TouchableWithoutFeedback
                onPress={() => {
                  setGender('NAM');
                }}>
                <View
                  style={
                    gender === 'NAM' ? styles.genderActive : styles.gender
                  }>
                  <Text style={styles.txtGender}>Nam</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => {
                  setGender('NU');
                }}>
                <View
                  style={gender === 'NU' ? styles.genderActive : styles.gender}>
                  <Text style={styles.txtGender}>Nữ</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>

          <View style={styles.row}>
            <Text style={styles.title}>Chiều cao</Text>
            <View style={styles.boxInput}>
              <TextInput style={styles.input} onChangeText={setNumHeight} />
              <Text style={styles.jaj}>cm</Text>
            </View>
          </View>

          <View style={styles.row}>
            <Text style={styles.title}>Cân nặng</Text>
            <View style={styles.boxInput}>
              <TextInput style={styles.input} onChangeText={setNumWeight} />
              <Text style={styles.jaj}>kg</Text>
            </View>
          </View>

          <TouchableWithoutFeedback onPress={onSubmit}>
            <View style={styles.btSubmit}>
              <Text style={styles.txtBt}>Cập nhật</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
      <Toast position="top" ref={refToast} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
  },
  wallet: {
    width: '94%',
    alignSelf: 'center',
    height: 500,
    backgroundColor: '#181c29',
    borderRadius: 10,
    paddingTop: 40,
  },
  textStyle: {
    fontWeight: '500',
    fontSize: 20,
    color: '#fff',
    lineHeight: 28,
  },
  containerStyle: {
    width: '86%',
    alignSelf: 'center',
    textAlign: 'center',
  },
  row: {
    flexDirection: 'row',
    width: '88%',
    height: 40,
    alignSelf: 'center',
    marginTop: 10,
    justifyContent: 'space-between',
  },
  title: {
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: 17,
    color: '#5cc87f',
  },
  boxGender: {
    width: 130,
    height: 30,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  genderActive: {
    width: 60,
    height: 30,
    backgroundColor: '#5cc87f',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  gender: {
    width: 60,
    height: 30,
    backgroundColor: '#c4c4c4',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  txtGender: {
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: 16,
    color: '#fff',
  },
  boxInput: {
    height: 30,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  input: {
    alignSelf: 'center',
    fontWeight: '600',
    fontSize: 17,
    color: '#5cc87f',
    backgroundColor: '#040505',
    width: 80,
    height: 30,
    marginRight: 10,
    borderRadius: 10,
    paddingHorizontal: 5,
    textAlign: 'right',
  },
  jaj: {
    width: 30,
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: 17,
    color: '#5cc87f',
  },
  btSubmit: {
    width: '60%',
    height: 40,
    alignSelf: 'center',
    backgroundColor: '#5cc87f',
    borderRadius: 20,
    justifyContent: 'center',
    marginTop: 130,
  },
  txtBt: {
    alignSelf: 'center',
    fontSize: 18,
    color: '#fff',
    fontWeight: '600',
  },
});

export default React.forwardRef(ModalWeigh);
