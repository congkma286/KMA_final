import {
  View,
  Modal,
  StyleSheet,
  Text,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useState, useImperativeHandle} from 'react';
import YoutubePlayer from 'react-native-youtube-iframe';
import convertTime from '../utils/convertTime';
var Sound = require('react-native-sound');

const {width} = Dimensions.get('window');

const whoosh = new Sound('whoosh2.mp3', Sound.MAIN_BUNDLE, error => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
  // loaded successfully
  console.log(
    'duration in seconds: ' +
      whoosh.getDuration() +
      'number of channels: ' +
      whoosh.getNumberOfChannels(),
  );

  // Play the sound with an onEnd callback
});

const ModalPrepare = (props, ref) => {
  const {handleAfter} = props;
  const [modalVisible, setModalVisible] = useState(false);
  const [playing, setPlaying] = useState(false);
  const [idVideo, setIdVideo] = useState('4qNALNWoGmI');
  const [countDown, setCountDown] = useState(10);

  const [data, setData] = useState({});

  const playerRef = React.useRef(null);

  const onShow = propsData => {
    setModalVisible(true);
    setPlaying(true);
    setCountDown(20);
    setData(propsData);
    setIdVideo(propsData?.id);

    whoosh.play(success => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
    });
  };

  const onHide = React.useCallback(() => {
    setModalVisible(false);
    setPlaying(false);
    setCountDown(0);
    handleAfter();
    whoosh.stop();
  }, [handleAfter]);

  useImperativeHandle(
    ref,
    () => ({
      onShow: onShow,
      onHide: onHide,
    }),
    [onHide],
  );

  const onStateChange = React.useCallback(state => {
    if (state === 'ended') {
      setPlaying(false);
    }
  }, []);

  const onPressPlusTime = () => {
    setCountDown(countDown + 20);
  };

  React.useEffect(() => {
    let funcCountDown = () => {};
    if (modalVisible) {
      funcCountDown = setInterval(() => {
        if (!countDown) {
          onHide();
          clearInterval(funcCountDown);
        } else {
          setCountDown(countDown - 1);
        }
      }, 1000);
    }

    return () => {
      clearInterval(funcCountDown);
    };
  }, [modalVisible, countDown, onHide]);

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <ImageBackground
        style={styles.centeredView}
        source={require('../assets/images/image_prepare.png')}>
        <View style={styles.modalView}>
          <SafeAreaView />
          <Text style={styles.title}>Nghỉ ngơi</Text>
          <Text style={styles.txtTime}>{convertTime(countDown)}</Text>
          <View style={styles.rowButton}>
            <TouchableWithoutFeedback onPress={onPressPlusTime}>
              <View style={styles.btCancel}>
                <Text style={styles.txtButton}>+20s</Text>
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={onHide}>
              <View style={styles.btSkip}>
                <Text style={styles.txtButton}>Bỏ qua</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <Text style={styles.txtNext}>Tiếp theo</Text>
          <View style={styles.txtRow}>
            <Text style={styles.titleNext}>{data?.title}</Text>
            <Text style={styles.titleNextRight}>
              {'x' + data?.rep || data?.title + 's' || ''}
            </Text>
          </View>
          <View style={styles.video}>
            <YoutubePlayer
              ref={playerRef}
              height={(9 * width) / 16}
              play={playing}
              videoId={idVideo}
              onChangeState={onStateChange}
            />
          </View>
        </View>
      </ImageBackground>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
  },
  modalView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  title: {
    alignSelf: 'center',
    fontSize: 30,
    fontWeight: '400',
    color: '#fff',
    fontFamily: 'Nunito-bold',
    marginTop: 110,
  },
  txtTime: {
    alignSelf: 'center',
    fontSize: 66,
    fontWeight: '600',
    color: '#fff',
    marginTop: 10,
  },
  rowButton: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 10,
    width: '60%',
    justifyContent: 'space-between',
  },
  btCancel: {
    alignSelf: 'center',
    width: '45%',
    height: 40,
    borderRadius: 20,
    backgroundColor: 'rgba(154,154,154,0.7)',
    justifyContent: 'center',
  },
  btSkip: {
    backgroundColor: '#5AC685',
    alignSelf: 'center',
    width: '45%',
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
  },
  txtButton: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
  },
  txtNext: {
    marginTop: 210,
    marginLeft: '3%',
    fontSize: 18,
    fontWeight: '800',
    color: '#fff',
    fontFamily: 'Nunito',
  },
  txtRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleNext: {
    marginLeft: '3%',
    fontSize: 20,
    fontWeight: '800',
    color: '#fff',
    marginBottom: 10,
    marginTop: 5,
    fontFamily: 'Nunito',
  },
  titleNextRight: {
    marginRight: '3%',
    fontSize: 20,
    fontWeight: '800',
    color: '#fff',
    marginBottom: 10,
    marginTop: 5,
    fontFamily: 'Nunito',
  },
  video: {
    paddingTop: 10,
    backgroundColor: 'rgba(255,255,255,0.8)',
    borderRadius: 10,
    paddingBottom: 10,
  },
});

export default React.forwardRef(ModalPrepare);
