import getHeaders from './getHeader';

const API = 'http://103.163.118.83/';

export const login = async body => {
  const response = await fetch(`${API}auth/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const register = async body => {
  const response = await fetch(`${API}auth/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const checkToken = async body => {
  const response = await fetch(`${API}auth/checkToken`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const sendOtp = async body => {
  const response = await fetch(`${API}auth/checkOtp`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const requireOtpAgain = async body => {
  const response = await fetch(`${API}auth/sendOtpAgain`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const sendForgotAccount = async body => {
  const response = await fetch(`${API}auth/sendForgotAccount`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const sendOtpForgot = async body => {
  const response = await fetch(`${API}auth/checkOtpForgot`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const changePassword = async body => {
  const response = await fetch(`${API}auth/changePass`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export default {
  login,
  register,
};
