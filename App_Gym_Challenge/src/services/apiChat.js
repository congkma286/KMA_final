import getHeaders from './getHeader';

const API = 'http://103.163.118.83/';

export const getListChat = async () => {
  const response = await fetch(`${API}chat/listChat`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });
  const responseJson = await response.json();
  return responseJson;
};
//http://localhost:3000/chat/gpttest
export const requestGPT = async ({token, prompt}) => {
  const response = await fetch(`${API}chat/gpt`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `${token}`,
    },
    body: JSON.stringify({prompt}),
  });
  const responseJson = await response.json();
  return responseJson;
};

export default {
  getListChat,
  requestGPT,
};
