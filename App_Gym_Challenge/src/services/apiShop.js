const API = 'http://103.163.118.83/';

export const getProduct = async () => {
  const response = await fetch(`${API}/shop/product`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });
  const responseJson = await response.json();
  return responseJson;
};

export const postAddProduct = async data => {
  const dataPost = {
    name: data.name,
    price: Number(data.price),
    img: data.img,
    link: data.link,
  };
  const response = await fetch(`${API}/shop/add`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(dataPost),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const postDelProduct = async _id => {
  const response = await fetch(`${API}/shop/delete`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({_id}),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const postEditProduct = async data => {
  const dataPost = {
    _id: data._id,
    name: data.name,
    price: Number(data.price),
    img: data.img,
    link: data.link,
  };
  const response = await fetch(`${API}/shop/update`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(dataPost),
  });
  const responseJson = await response.json();
  return responseJson;
};
