const getHeaders = token => ({
  'Content-Type': 'application/json',
  Accept: 'application/json',
  Authorization: `Bearer ${token}`,
  Referer: 'https://app.onluyen.vn',
});

export default getHeaders;
