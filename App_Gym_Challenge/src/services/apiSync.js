const API = 'http://103.163.118.83/';

export const apiSync = async ({token}) => {
  const response = await fetch(`${API}/sync/check`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `${token}`,
    },
  });
  const responseJson = await response.json();
  return responseJson;
};

export const apiUpdateSync = async ({token, body}) => {
  const response = await fetch(`${API}/sync/update`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `${token}`,
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};

export const apiAddSync = async ({token, body}) => {
  const response = await fetch(`${API}/sync/add`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `${token}`,
    },
    body: JSON.stringify(body),
  });
  const responseJson = await response.json();
  return responseJson;
};
