import {createSlice} from '@reduxjs/toolkit';

const dataSlice = createSlice({
  name: 'data',
  initialState: {
    myChallenge: [],
    otherChallenge: [],
    listIdChallenge: [],
    reload: 0,
    time: 0,
  },
  reducers: {
    setMyChallenge(state, action) {
      state.myChallenge = action.payload;
    },
    setOtherChallenge(state, action) {
      state.otherChallenge = action.payload;
    },
    setListIdChallenge(state, action) {
      state.listIdChallenge = action.payload;
    },

    requestReload(state, action) {
      state.reload = state.reload + 1;
    },

    startTime(state, action) {
      state.time = action.payload;
    },
  },
});

export const {
  setMyChallenge,
  setOtherChallenge,
  setListIdChallenge,
  requestReload,
  startTime,
} = dataSlice.actions;
export default dataSlice.reducer;
