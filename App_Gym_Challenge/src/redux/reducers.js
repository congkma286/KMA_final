import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {authReducer, otherReducer} from './reducers';
import dataReducer from './dataSlice';

const reducers = {
  data: dataReducer,
};

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: undefined,
};

const rootReducer = combineReducers({
  auth: persistReducer(persistConfig, reducers),
});

export default rootReducer;
