import {createSlice} from '@reduxjs/toolkit';
import {setWeigh} from '../utils/strorage';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLogin: false,
    nameUser: '',
    _id: '',
    name: '',
    role: '',
  },
  reducers: {
    changeStatusLogin(state, action) {
      state.isLogin = action.payload;
    },
    changeInfo(state, action) {
      state.name = action.payload.name;
      state.nameUser = action.payload.nameUser;
      state._id = action.payload._id;
      state.role = action.payload.role;
      setWeigh({
        gender: action.payload.gender,
        weigh: action.payload.weight,
        height: action.payload.height,
      });
    },
  },
});

export const {changeStatusLogin, changeInfo} = authSlice.actions;
export default authSlice.reducer;
