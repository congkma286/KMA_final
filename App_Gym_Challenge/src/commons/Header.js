import {View, Text, StyleSheet, Dimensions} from 'react-native';
import React from 'react';
import FastImage from 'react-native-fast-image';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const {width} = Dimensions.get('window');

const Header = props => {
  const {
    type,
    title,
    iconRight,
    textColor = '#000',
    color = '#000',
    onPressLeft,
    onPressRight,
  } = props;
  const navigation = useNavigation();

  const styles = StyleSheet.create({
    container: {
      width: '100%',
      height: 40,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: '5%',
      alignItems: 'center',
    },
    iconLeft: {
      width: 20,
      height: 20,
      alignSelf: 'center',
    },
    title: {
      alignSelf: 'center',
      fontSize: 17,
      fontWeight: '700',
      fontFamily: 'Nunito-Bold',
      width: width * 0.9 - 50,
      textAlign: 'center',
      color: textColor,
    },
  });

  const goBack = () => {
    try {
      if (onPressLeft) {
        onPressLeft();
      } else {
        navigation.goBack();
      }
    } catch (error) {
      console.log(error);
    }
  };

  switch (type) {
    case 'default':
      return (
        <View style={styles.container}>
          <View style={styles.iconLeft} />
          <Text numberOfLines={1} style={styles.title}>
            {title}
          </Text>
          <View style={styles.iconLeft} />
        </View>
      );
    case 'iconLeft':
      return (
        <View style={styles.container}>
          <TouchableWithoutFeedback
            hitSlop={{top: 10, right: 10, left: 10, bottom: 10}}
            onPress={goBack}
            style={styles.center}>
            <FastImage
              tintColor={color}
              style={styles.iconLeft}
              source={require('../assets/icons/iconBack.png')}
            />
          </TouchableWithoutFeedback>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.iconLeft} />
        </View>
      );
    case 'iconRight':
      return (
        <View style={styles.container}>
          <View style={styles.iconLeft} />
          <Text style={styles.title}>{title}</Text>
          <TouchableWithoutFeedback
            onPress={onPressRight}
            hitSlop={{top: 10, right: 10, left: 10, bottom: 10}}
            style={styles.center}>
            {iconRight ? (
              iconRight()
            ) : (
              <FastImage
                tintColor={color}
                style={styles.iconLeft}
                source={require('../assets/icons/iconRightDefault.png')}
              />
            )}
          </TouchableWithoutFeedback>
        </View>
      );
    case 'full':
      return (
        <View style={styles.container}>
          <TouchableWithoutFeedback
            hitSlop={{top: 10, right: 10, left: 10, bottom: 10}}
            onPress={goBack}
            style={styles.center}>
            <FastImage
              style={styles.iconLeft}
              tintColor={color}
              source={require('../assets/icons/iconBack.png')}
            />
          </TouchableWithoutFeedback>
          <Text style={styles.title}>{title}</Text>
          <TouchableWithoutFeedback
            onPress={onPressRight}
            hitSlop={{top: 10, right: 10, left: 10, bottom: 10}}
            style={styles.center}>
            {iconRight ? (
              iconRight()
            ) : (
              <FastImage
                style={styles.iconLeft}
                tintColor={color}
                source={require('../assets/icons/iconRightDefault.png')}
              />
            )}
          </TouchableWithoutFeedback>
        </View>
      );
    default:
      return <></>;
  }
};

export default Header;
