import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  SafeAreaView,
  TextInput,
  Dimensions,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import React, {useState, useRef} from 'react';
import Header from '../../commons/Header';
import Toast from 'react-native-easy-toast';
import jwtDecode from 'jwt-decode';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {changePassword} from '../../services/apiAuth';

const {height} = Dimensions.get('window');

const Index = () => {
  const [oldPass, setOldPass] = useState('');
  const [newPass, setNewPass] = useState('');
  const [reNewPass, setReNewPass] = useState('');

  const refToast = useRef(null);

  const submit = async () => {
    if (!oldPass) {
      refToast.current.show('Yêu cầu nhập mật khẩu hiển tại', 2000);
      return;
    }
    if (oldPass.length < 6) {
      refToast.current.show('Mật khẩu hiện tại không hợp lệ', 2000);
      return;
    }
    if (!newPass) {
      refToast.current.show('Yêu cầu nhập mật khẩu mới', 2000);
      return;
    }
    if (newPass.length < 6) {
      refToast.current.show('Mật khẩu mới không hợp lệ', 2000);
      return;
    }
    if (!reNewPass) {
      refToast.current.show('Yêu cầu nhập nhập lại mật khẩu mới', 2000);
      return;
    }
    if (reNewPass.length < 6) {
      refToast.current.show(
        'Mật khẩu nhập lại mật khẩu mới không hợp lệ',
        2000,
      );
      return;
    }
    if (reNewPass !== newPass) {
      refToast.current.show(
        'Mật khẩu mới và mật khẩu nhập lại không khớp',
        2000,
      );
      return;
    }
    const token = await AsyncStorage.getItem('@token');
    const {_id} = await jwtDecode(token);
    const res = await changePassword({
      _id,
      password: oldPass,
      newPassword: newPass,
    });
    console.log('bach res', res);
    if (res.status === 1) {
      Alert.alert('', 'Đổi mật khẩu thành công');
    } else {
      Alert.alert('', 'Đổi mật khẩu không thành công');
    }
  };

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.container}>
      <SafeAreaView />
      <Header
        type={'iconLeft'}
        title={'Đổi mật khẩu'}
        color={'#fff'}
        textColor={'#fff'}
      />
      <Text style={styles.titleInput}>Nhập mật khẩu hiện tại</Text>
      <TextInput
        value={oldPass}
        onChangeText={setOldPass}
        style={styles.input}
        secureTextEntry
      />
      <Text style={styles.titleInput}>Nhập mật mới</Text>
      <TextInput
        value={newPass}
        onChangeText={setNewPass}
        style={styles.input}
        secureTextEntry
      />
      <Text style={styles.titleInput}>Nhập lại mật khẩu mới</Text>
      <TextInput
        value={reNewPass}
        onChangeText={setReNewPass}
        style={styles.input}
        secureTextEntry
      />
      <TouchableWithoutFeedback onPress={submit}>
        <View style={styles.btSubmit}>
          <Text style={styles.txtBT}>Đổi mật khẩu</Text>
        </View>
      </TouchableWithoutFeedback>

      <Toast position="top" ref={refToast} />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  titleInput: {
    fontSize: 17,
    color: '#5AC685',
    fontWeight: '600',
    paddingLeft: '5%',
    marginTop: 16,
  },
  input: {
    alignSelf: 'center',
    width: '90%',
    alignSelf: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#fff',
    height: 40,
    marginTop: 10,
    paddingHorizontal: '5%',
    color: '#5AC685',
  },
  btSubmit: {
    width: '70%',
    alignSelf: 'center',
    position: 'absolute',
    top: height - 120,
    height: 40,
    borderRadius: 40,
    justifyContent: 'center',
    backgroundColor: '#5AC685',
  },
  txtBT: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '700',
  },
});

export default Index;
