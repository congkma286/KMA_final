import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Dimensions,
  TouchableWithoutFeedback,
  Alert,
  ImageBackground,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import Header from '../../commons/Header';
import {useNavigation, useRoute} from '@react-navigation/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCheck, faStar, faBars} from '@fortawesome/free-solid-svg-icons';
import Progress from '../../components/Progress';
import {useDispatch} from 'react-redux';
import {startTime} from '../../redux/dataSlice';
import {getLikeEx, setLikeEx} from '../../utils/strorage';
import ModalLevel from '../../components/ModalLevel';

const {width, height} = Dimensions.get('window');

const Index = props => {
  const navigation = useNavigation();
  // const route = useRoute();
  const route = props?.route;
  const [data, setData] = useState(route?.params?.data);
  const [level, setLevel] = useState(route?.params?.level);
  const currentIndex = data?.day || 0;

  const [listDaily, setListDaily] = useState([]);
  const [like, setLike] = useState(false);

  const refModalLevel = useRef(null);

  const loadAgain = ({data, level}) => {
    setData(data);
    setLevel(level);
  };

  const onPressItemDaily = ({index}) => {
    return;
    navigation.navigate('Prepare', {
      day: index + 1,
      type: data.type,
      level: level,
      id: data.id,
      progress: data.progress,
      title: data.title,
    });
  };

  const onPressStart = () => {
    navigation.navigate('Prepare', {
      day: currentIndex,
      type: data.type,
      level: level,
      id: data.id,
      progress: data.progress,
      title: data.title,
    });
  };

  const fakeData = async () => {
    const array = [];
    for (let index = 0; index < 30; index++) {
      array.push({index});
    }
    setListDaily(array);

    const dataLike = await getLikeEx();
    if (dataLike?.includes(data?.id)) {
      setLike(true);
    }
  };

  useEffect(() => {
    fakeData();
  }, []);

  const checkLike = async () => {
    const dataLike = await getLikeEx();
    if (like) {
      if (dataLike?.length > 0) {
        const dataLikeNew1 = dataLike?.filter(a => a != data?.id);
        await setLikeEx([...dataLikeNew1, data?.id]);
      } else {
        await setLikeEx([data?.id]);
      }
    } else {
      const dataLikeNew = dataLike?.filter(a => a != data?.id);
      await setLikeEx(dataLikeNew);
    }
  };

  useEffect(() => {
    checkLike();
  }, [like]);

  const getStyleItemDaily = i => {
    if (i + 1 < currentIndex) {
      return styles.itemDailyDone;
    } else if (i + 1 === currentIndex) {
      return styles.itemDaily;
    } else {
      return styles.itemDailySlacking;
    }
  };

  const onPressIconHeader = () => {
    refModalLevel.current.onShow();
  };

  const getDayItemDaily = i => {
    if (i + 1 < currentIndex) {
      return (
        <FontAwesomeIcon
          style={styles.iconCheck}
          color={'#fff'}
          size={20}
          icon={faCheck}
        />
      );
    } else if (i + 1 === currentIndex) {
      return <Text style={styles.txtItemDailyNow}>{i + 1}</Text>;
    } else {
      return <Text style={styles.txtItemDaily}>{i + 1}</Text>;
    }
  };

  const renderItemDaily = ({item, index}) => {
    return (
      <TouchableWithoutFeedback onPress={() => onPressItemDaily({index})}>
        <View style={getStyleItemDaily(index)}>
          {/* <Text style={styles.txtItemDaily}>1</Text> */}
          {getDayItemDaily(index)}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderIconRight = () => {
    return (
      <FontAwesomeIcon
        style={styles.iconLevel}
        color={'#77C38A'}
        size={26}
        icon={faBars}
      />
    );
  };

  return (
    <ImageBackground
      style={styles.container}
      source={require('../../assets/images/hinhnen2.png')}>
      <StatusBar barStyle={'light-content'} />
      <SafeAreaView />
      <Header
        type={'full'}
        title={data?.title || ''}
        color={'#fff'}
        textColor={'#fff'}
        iconRight={renderIconRight}
        onPressRight={onPressIconHeader}
      />
      <FlatList
        numColumns={6}
        data={listDaily}
        renderItem={renderItemDaily}
        keyExtractor={(_, i) => i + ''}
        contentContainerStyle={styles.listDaily}
        style={styles.styFlatlist}
      />
      <View style={styles.rowText}>
        <Text style={styles.txtLevel}>{level?.title}</Text>
        <Text style={styles.txtLevel}>{data?.progress}%</Text>
      </View>
      <Progress
        height={8}
        top={5}
        width={width * 0.9}
        progress={data?.progress || 0}
        color={'#77C38A'}
      />

      <TouchableWithoutFeedback onPress={() => setLike(!like)}>
        <View style={styles.btLike}>
          <FontAwesomeIcon
            style={styles.iconStar}
            color={like ? '#FAFF00' : '#c4c4c4'}
            size={36}
            icon={faStar}
          />
        </View>
      </TouchableWithoutFeedback>

      <TouchableWithoutFeedback onPress={onPressStart}>
        <View style={styles.btStart}>
          <Text style={styles.txtBtStart}>Bắt đầu</Text>
        </View>
      </TouchableWithoutFeedback>

      <ModalLevel data={data} ref={refModalLevel} loadAgain={loadAgain} />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  listDaily: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  styFlatlist: {
    height: 300,
    flexGrow: 0,
  },
  itemDailyDone: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#77C38A',
    justifyContent: 'center',
    marginHorizontal: 5,
    marginTop: 5,
  },
  itemDaily: {
    width: 50,
    height: 50,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: '#77C38A',
    justifyContent: 'center',
    marginHorizontal: 5,
    marginTop: 5,
  },
  itemDailySlacking: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    marginHorizontal: 5,
    marginTop: 5,
    backgroundColor: '#c4c4c4',
    opacity: 0.7,
  },
  txtItemDailyNow: {
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '700',
    color: '#77C38A',
  },
  txtItemDaily: {
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '600',
    color: '#fff',
  },
  iconCheck: {
    alignSelf: 'center',
  },
  txtLevel: {
    color: '#fff',
    fontSize: 15,
    fontWeight: '800',
  },
  rowText: {
    width: '90%',
    justifyContent: 'space-between',
    alignSelf: 'center',
    flexDirection: 'row',
  },
  btStart: {
    width: '80%',
    height: 50,
    borderRadius: 40,
    backgroundColor: '#77C38A',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 20,
    position: 'absolute',
    top: height - 110,
  },
  txtBtStart: {
    alignSelf: 'center',
    fontSize: 18,
    fontw: '800',
    color: '#fff',
    fontFamily: 'Nunito-Bold',
  },
  btLike: {
    width: 55,
    height: 55,
    borderRadius: 55,
    backgroundColor: '#777777',
    position: 'absolute',
    top: height - 180,
    alignSelf: 'flex-end',
    right: 20,
    justifyContent: 'center',
  },
  iconStar: {
    alignSelf: 'center',
  },
  iconLevel: {
    alignSelf: 'center',
  },
});

export default Index;
