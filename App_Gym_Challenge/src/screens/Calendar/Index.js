import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {
  Calendar,
  CalendarList,
  Agenda,
  CalendarUtils,
} from 'react-native-calendars';
import useLocaleCalendars from '../../utils/useLocaleCalendars';
import moment from 'moment';
import FastImage from 'react-native-fast-image';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faCheck,
  faFire,
  faClock,
  faGlassWater,
} from '@fortawesome/free-solid-svg-icons';
import {useMemo} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import convertTime from '../../utils/convertTime';
import {useCallback} from 'react';
import {useSelector} from 'react-redux';

const {width} = Dimensions.get('window');

const Index = () => {
  const INITIAL_DATE = moment(now).format('YYYY-MM-DD');
  const now = new Date();
  const config = useLocaleCalendars();
  const {reload} = useSelector(state => state.data);
  config.setup();
  const [initialDate, setInitialDate] = useState(
    moment(now).format('YYYY-MM-DD'),
  );
  const [dataHistory, setDataHistory] = useState([]);
  const [historyCurrent, setHistoryCurrent] = useState([]);

  const [historyDrink, setDataHistoryDrink] = useState([]);
  const [historyDrinkCurrent, setHistoryDrinkCurrent] = useState({});

  const [totalCalo, setTotalCalo] = useState(0);
  const [totalTime, setTotalTime] = useState(0);

  const viewRef = useRef(null);

  const onDayPress = async day => {
    setInitialDate(day.dateString);
    const idDay = moment(day.timestamp).format('DD/MM/YYYY');
    const data = dataHistory.filter(a => a.timeDay === idDay);
    setHistoryCurrent(data);
    getTotalDay(data);
    const data2 = historyDrink.filter(a => a.timeDay === idDay);
    if (data2.length > 0) {
      setHistoryDrinkCurrent(data2[0]);
    } else {
      setHistoryDrinkCurrent({});
    }
  };

  const getTotalDay = data => {
    if (data?.length > 0) {
      let tcalo = 0;
      let tTime = 0;
      data.map(a => {
        tcalo += a?.calo;
        tTime += a?.timeExercising;
      });
      setTotalCalo(tcalo);
      setTotalTime(tTime);
    } else {
      setTotalCalo(0);
      setTotalTime(0);
    }
  };

  const getData = async () => {
    const dataStroge = await AsyncStorage.getItem('@history');
    if (dataStroge !== null) {
      const data = JSON.parse(dataStroge);
      setDataHistory(data);

      const idDay = moment(now).format('DD/MM/YYYY');
      const dataH = data.filter(a => a.timeDay === idDay);
      setHistoryCurrent(dataH);
      getTotalDay(dataH);
    }
    const dataStrogeDrink = await AsyncStorage.getItem('@historyDrink');
    if (dataStrogeDrink !== null) {
      const data2 = JSON.parse(dataStrogeDrink);
      setDataHistoryDrink(data2);

      const idDay = moment(now).format('DD/MM/YYYY');
      const dataH2 = data2.filter(a => a.timeDay === idDay);
      if (dataH2.length > 0) {
        setHistoryDrinkCurrent(dataH2[0]);
      } else {
        setHistoryDrinkCurrent({});
      }
    }
  };

  useEffect(() => {
    getData();
  }, [reload]);

  const marked = useMemo(() => {
    return {
      [initialDate]: {
        selected: true,
        disableTouchEvent: true,
        selectedColor: '#5AC685',
        selectedTextColor: '#fff',
      },
    };
  }, [initialDate]);

  const Arrow = ({direction}) => {
    if (direction === 'left') {
      return (
        <FastImage
          style={styles.iconArrowLeft}
          tintColor={'#fff'}
          source={require('../../assets/icons/icon_arrow_left.png')}
        />
      );
    } else {
      return (
        <FastImage
          style={styles.iconArrowRight}
          tintColor={'#fff'}
          source={require('../../assets/icons/icon_arrow_left.png')}
        />
      );
    }
  };

  const renderEmpty = () => {
    return (
      <View>
        <Text style={styles.txtEmpty}>Bạn không có lịch sử tập luyện</Text>
      </View>
    );
  };

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.detailByDate}>
        <View style={styles.itemDetailDate}>
          <FontAwesomeIcon
            style={styles.iconDetailByDate}
            color={'#5AC685'}
            size={24}
            icon={faCheck}
          />
          <Text style={styles.txtDetailDate}>{item?.title}</Text>
          <Text style={styles.txtTypelDate}>Ngày {item?.day}</Text>
        </View>
        <View style={styles.itemDetailDate}>
          <FontAwesomeIcon
            style={styles.iconDetailByDate}
            color={'#5AC685'}
            size={24}
            icon={faFire}
          />
          <Text style={styles.txtDetailDate}>{item?.calo}</Text>
          <Text style={styles.txtTypelDate}>KCAL</Text>
        </View>
        <View style={styles.itemDetailDate}>
          <FontAwesomeIcon
            style={styles.iconDetailByDate}
            color={'#5AC685'}
            size={24}
            icon={faClock}
          />
          <Text style={styles.txtDetailDate}>
            {convertTime(item?.timeExercising)}
          </Text>
          <Text style={styles.txtTypelDate}>THỜI LƯỢNG</Text>
        </View>
      </View>
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.detailByDate}>
        <View style={styles.itemDetailDate}>
          <FontAwesomeIcon
            style={styles.iconDetailByDate}
            color={'#5AC685'}
            size={24}
            icon={faGlassWater}
          />
          <Text style={styles.txtDetailDate}>
            {historyDrinkCurrent?.numDrink || 0}{' '}
          </Text>
          <Text style={styles.txtTypelDate}>Lượng nước</Text>
        </View>
        <View style={styles.itemDetailDate}>
          <FontAwesomeIcon
            style={styles.iconDetailByDate}
            color={'#5AC685'}
            size={24}
            icon={faFire}
          />
          <Text style={styles.txtDetailDate}>{totalCalo}</Text>
          <Text style={styles.txtTypelDate}>Tổng KCAL</Text>
        </View>
        <View style={styles.itemDetailDate}>
          <FontAwesomeIcon
            style={styles.iconDetailByDate}
            color={'#5AC685'}
            size={24}
            icon={faClock}
          />
          <Text style={styles.txtDetailDate}>{convertTime(totalTime)}</Text>
          <Text style={styles.txtTypelDate}>TỔNG THỜI LƯỢNG</Text>
        </View>
      </View>
    );
  };

  const History = useCallback(
    () => (
      <FlatList
        data={historyCurrent}
        ListHeaderComponent={renderHeader}
        renderItem={renderItem}
        ListEmptyComponent={renderEmpty}
        keyExtractor={({_, index}) => index + ''}
        ListFooterComponent={() => <View style={styles.footer} />}
        showsVerticalScrollIndicator={false}
      />
    ),
    [historyCurrent, historyDrinkCurrent],
  );

  return (
    <ImageBackground
      ref={viewRef}
      style={styles.container}
      source={require('../../assets/images/hinhnen2.png')}>
      <SafeAreaView />
      <Text style={styles.titleScreen}>Lịch</Text>
      <Calendar
        style={styles.calendar}
        initialDate={initialDate}
        current={initialDate}
        onDayPress={onDayPress}
        monthFormat={'yyyy MM'}
        renderArrow={direction => <Arrow direction={direction} />}
        disableMonthChange={true}
        onMonthChange={month => {
          setInitialDate(month.dateString);
        }}
        onPressArrowLeft={subtractMonth => subtractMonth()}
        onPressArrowRight={addMonth => addMonth()}
        disableAllTouchEventsForDisabledDays={true}
        renderHeader={date => {
          return <Text style={styles.txtDateInit}>{initialDate}</Text>;
        }}
        markedDates={marked}
        theme={{
          backgroundColor: '#303545',
          calendarBackground: '#303545',
          todayTextColor: '#5AC685',
          dayTextColor: '#ffffff',
          textDisabledColor: '#595E6A',
        }}
      />

      <History />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  titleScreen: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '700',
    color: '#5AC685',
  },
  calendar: {
    marginTop: 30,
    backgroundColor: '#303545',
  },
  iconArrowLeft: {
    width: 20,
    height: 20,
  },
  iconArrowRight: {
    width: 20,
    height: 20,
    transform: [{rotate: '180deg'}],
  },
  txtDateInit: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 16,
  },
  detailByDate: {
    width: '96%',
    height: 120,
    backgroundColor: '#303545',
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    paddingHorizontal: '3%',
    justifyContent: 'space-between',
  },
  itemDetailDate: {
    flex: 1,
    alignSelf: 'center',
  },
  txtDetailDate: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '700',
    color: '#fff',
    marginVertical: 7,
    textAlign: 'center',
  },
  iconDetailByDate: {
    alignSelf: 'center',
  },
  txtTypelDate: {
    alignSelf: 'center',
    fontSize: 12,
    fontWeight: '700',
    color: '#92969D',
    textAlign: 'center',
  },
  footer: {
    width: '100%',
    height: 110,
  },
  imgEmpty: {
    width: width * 0.7,
    height: (width * 0.7 * 310) / 553,
    alignSelf: 'center',
    marginTop: 30,
  },
  txtEmpty: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '700',
    color: '#fff',
    textAlign: 'center',
    marginTop: 120,
  },
  blockWater: {
    width: '96%',
    height: 100,
    backgroundColor: '#303545',
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  blockWaterChild: {
    width: '33%',
    height: 80,
    justifyContent: 'center',
    paddingTop: 5,
  },
  txtDrink: {
    alignSelf: 'center',
    marginTop: 8,
    fontSize: 16,
    color: '#5AC685',
    fontWeight: '600',
  },
  drinkToday: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '10%',
    marginTop: 10,
    height: 30,
  },
  txt3: {
    alignSelf: 'center',
    fontSize: 15,
    color: '#5AC685',
    fontWeight: '600',
  },
  btDrink: {
    width: 100,
    height: 30,
    borderRadius: 30,
    backgroundColor: '#5AC685',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  txt4: {
    alignSelf: 'center',
    fontSize: 14,
    color: '#fff',
    fontWeight: '600',
  },
  txt5: {
    alignSelf: 'center',
    fontSize: 15,
    color: '#5AC685',
    fontWeight: '600',
    marginTop: 15,
  },
  txt6: {
    fontSize: 17,
    color: '#fff',
  },
});

export default Index;
