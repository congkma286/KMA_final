import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Dimensions,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import React, {useEffect, useRef, useState, useCallback} from 'react';
import Header from '../../commons/Header';
import {useNavigation, useRoute} from '@react-navigation/native';
import FastImage from 'react-native-fast-image';
import ModalBottom from '../../components/ModalBottom';
import YoutubePlayer from 'react-native-youtube-iframe';
import getTextLevel from '../../utils/getTextLevel';
import getIconExercise from '../../utils/getIconExercise';
import {getRep, getTime} from '../../utils/getQuantityExercise';
import getPrepare from '../../utils/getPrepare';
import {startTime} from '../../redux/dataSlice';
import {useDispatch} from 'react-redux';
import {ImageBackground} from 'react-native';

const {width, height} = Dimensions.get('window');

const a = [
  {
    title: 'Bật nhảy',
    time: 22,
    rep: 0,
    calo: 5,
    idVideo: '2W4ZNSwoW_4',
    idExercise: '1',
    description: `Bắt đầu đứng hai chân để gần nhau, tay buông xuống để hai bên, sau đó nhảy lên chân dạng ra và để tay lên cao trên đầu. Trở lại vị trí bắt đầu rồi lặp lại
    
    Bài tập này tập luyện cho toàn thân và có tác dụng với tất cả các nhóm cơ lớn`,
  },
  {
    title: 'Leo núi',
    time: 0,
    rep: 12,
    calo: 5,
    idVideo: 'K0R4snqCbRA',
    idExercise: '2',
    description: `Bắt đầu đứng hai chân để gần nhau, tay buông xuống để hai bên, sau đó nhảy lên chân dạng ra và để tay lên cao trên đầu. Trở lại vị trí bắt đầu rồi lặp lại
    
    Bài tập này tập luyện cho toàn thân và có tác dụng với tất cả các nhóm cơ lớn`,
  },
  {
    title: 'Chùng chân ngang',
    time: 0,
    rep: 12,
    calo: 5,
    idVideo: 'lqOuqA1Ii7U',
    idExercise: '3',
    description: `Bắt đầu đứng hai chân để gần nhau, tay buông xuống để hai bên, sau đó nhảy lên chân dạng ra và để tay lên cao trên đầu. Trở lại vị trí bắt đầu rồi lặp lại
    
    Bài tập này tập luyện cho toàn thân và có tác dụng với tất cả các nhóm cơ lớn`,
  },
  {
    title: 'Chống đầu và xoay người',
    time: 0,
    rep: 12,
    calo: 5,
    idVideo: 'lqOuqA1Ii7U',
    idExercise: '1',
    description: `Bắt đầu đứng hai chân để gần nhau, tay buông xuống để hai bên, sau đó nhảy lên chân dạng ra và để tay lên cao trên đầu. Trở lại vị trí bắt đầu rồi lặp lại
    
    Bài tập này tập luyện cho toàn thân và có tác dụng với tất cả các nhóm cơ lớn`,
  },
];

const Index = a => {
  const navigation = useNavigation();
  // const route = useRoute();
  const route = a?.route;
  const playerRef = useRef(null);
  const refModalBottom = useRef(null);
  const props = route.params;
  const {day, level} = props;
  console.log('route.params', route.params);
  const initData = getPrepare(props);

  const [listExercise, setListExercise] = useState(initData);
  const [currentData, setCurrentData] = useState();
  //need change to id video
  const [isShowVideo, setIsShowVideo] = useState(false);
  const [playing, setPlaying] = useState(false);

  const [totalCalo, setTotalCalo] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    let calo = 0;
    initData.map(a => {
      calo = calo + a.calo;
    });
    setTotalCalo(calo);
  }, []);

  const onStateChange = useCallback(state => {
    if (state === 'ended') {
      setPlaying(false);
    }
  }, []);

  const onPressItemExercise = item => {
    setIsShowVideo(true);
    setCurrentData(item);
    setTimeout(() => {
      refModalBottom.current.onShow();
    }, 500);
  };

  const onPressStart = () => {
    dispatch(startTime(new Date().getTime()));
    navigation.navigate('Exercising', {data: listExercise, parent: props});
  };

  const renderItemExercise = ({item}) => {
    return (
      <TouchableWithoutFeedback onPress={() => onPressItemExercise(item)}>
        <View style={styles.itemExercise}>
          <FastImage
            style={styles.iconItemExercise}
            source={getIconExercise(item.id)}
          />
          <View style={styles.contentItemExercise}>
            <Text style={styles.txtNameItemExercise}>{item.title}</Text>
            <Text style={styles.numItemExercise}>
              {item?.time !== 0 ? getTime(item.time) : getRep(item.rep)}
            </Text>
          </View>
          <FastImage
            tintColor={'#fff'}
            style={styles.iconRight}
            source={require('../../assets/icons/iconBack.png')}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.container}>
      <StatusBar barStyle={'light-content'} />
      <SafeAreaView />
      <Header
        type={'iconLeft'}
        title={`Ngày ${day}`}
        color={'#fff'}
        textColor={'#fff'}
        // iconRight={renderIconRight}
        // onPressRight={onPressIconHeader}
      />

      <View style={styles.rowInfoDetailExercise}>
        <View style={styles.itemInfoDetailExercise}>
          <Text style={styles.txtInfoDetailExercise}>{totalCalo}</Text>
          <Text style={styles.txtInfoDetailExercise}>KCAL</Text>
        </View>
        <View style={styles.itemInfoDetailExercise}>
          <Text style={styles.txtInfoDetailExercise}>3-5</Text>
          <Text style={styles.txtInfoDetailExercise}>PHÚT</Text>
        </View>
        <View style={styles.itemInfoDetailExercise}>
          <Text style={styles.txtInfoDetailExercise}>
            {getTextLevel(level)}
          </Text>
          <Text style={styles.txtInfoDetailExercise}>CẤP ĐỘ</Text>
        </View>
      </View>

      <Text style={styles.titleExercise}>BÀI TẬP</Text>
      <FlatList
        data={listExercise}
        renderItem={renderItemExercise}
        keyExtractor={(_, i) => i + ''}
        ListFooterComponent={() => (
          <TouchableWithoutFeedback onPress={onPressStart}>
            <View style={styles.btStart}>
              <Text style={styles.txtBtStart}>Bắt đầu</Text>
            </View>
          </TouchableWithoutFeedback>
        )}
      />

      <ModalBottom
        callBackHide={() => {
          setPlaying(false);
          setIsShowVideo(false);
        }}
        snapPoints={['90%']}
        ref={refModalBottom}>
        <View style={styles.modalBottom}>
          <Text style={styles.titleModalBottom}>Tổng quan bài tập</Text>
          {isShowVideo && (
            <YoutubePlayer
              ref={playerRef}
              height={(9 * width) / 16}
              play={playing}
              videoId={currentData?.id}
              onChangeState={onStateChange}
            />
          )}
          <Text style={styles.txtNameItem}>{currentData?.title}</Text>
          <Text style={styles.txtConterntModal}>{currentData?.dep}</Text>
        </View>
      </ModalBottom>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#181c29',
  },
  rowInfoDetailExercise: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
  itemInfoDetailExercise: {
    flex: 1,
    justifyContent: 'center',
  },
  txtInfoDetailExercise: {
    alignSelf: 'center',
    fontSize: 17,
    fontWeight: '700',
    color: '#fff',
    marginBottom: 5,
    fontFamily: 'Nunito',
  },
  titleExercise: {
    fontSize: 17,
    fontWeight: '700',
    color: '#fff',
    fontFamily: 'Nunito',
    marginTop: 20,
    marginLeft: '5%',
  },
  itemExercise: {
    width: '90%',
    height: 80,
    marginTop: 14,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  iconItemExercise: {
    width: 80,
    height: 80,
    borderRadius: 10,
    alignSelf: 'center',
  },
  contentItemExercise: {
    width: width * 0.9 - 80 - 20,
    paddingLeft: '5%',
    justifyContent: 'center',
  },
  txtNameItemExercise: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '700',
  },
  numItemExercise: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '700',
    marginTop: 2,
  },
  listFooterComponentStyle: {
    width: '100%',
    height: 60,
  },
  iconRight: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    transform: [{rotate: '180deg'}],
  },
  btStart: {
    width: '80%',
    height: 50,
    borderRadius: 40,
    backgroundColor: '#77C38A',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 40,
  },
  txtBtStart: {
    alignSelf: 'center',
    fontSize: 18,
    fontw: '800',
    color: '#fff',
    fontFamily: 'Nunito-Bold',
  },
  modalBottom: {
    flex: 1,
    backgroundColor: '#303545',
  },
  titleModalBottom: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
    marginTop: 20,
    fontFamily: 'Nunito',
    marginBottom: 20,
  },
  txtNameItem: {
    marginLeft: '5%',
    fontFamily: 'Nunito',
    fontSize: 18,
    fontWeight: '700',
    color: '#5AC685',
    marginTop: 10,
  },
  txtConterntModal: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 5,
    color: '#FFF',
    fontFamily: 'Nunito',
    fontSize: 16,
  },
});

export default Index;
