import {
  View,
  SafeAreaView,
  StyleSheet,
  FlatList,
  Dimensions,
  Text,
  TouchableWithoutFeedback,
  Linking,
  ImageBackground,
  TouchableOpacity,
  Modal,
  Alert,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import Header from '../../commons/Header';
import FastImage from 'react-native-fast-image';
import {getProduct, postDelProduct} from '../../services/apiShop';
import {useSelector} from 'react-redux';
import ModalAddProduct from '../../components/ModalAddProduct';
import ModalEditProduct from '../../components/ModalEditProduct';

const {width} = Dimensions.get('window');

const Index = () => {
  const {role} = useSelector(state => state.auth);
  const [data, setData] = useState([]);

  const [modalVisible, setModalVisible] = useState(false);

  const [currentItem, setCurrentItem] = useState();

  const refModalAddProduct = useRef(null);
  const refModalEditProduct = useRef(null);

  const getData = async () => {
    try {
      const res = await getProduct();
      if (res.status === 1) {
        setData(res.data);
      }
    } catch (error) {}
  };

  useEffect(() => {
    getData();
  }, []);

  const linkToShop = link => {
    try {
      Linking.openURL(link);
    } catch (error) {}
  };

  const onLongPress = item => {
    if (role === 'admin') {
      setModalVisible(true);
      setCurrentItem(item);
    }
  };

  const onPressAddNew = () => {
    refModalAddProduct.current.onShow();
  };

  const handleDelete = async () => {
    const res = await postDelProduct({_id: currentItem._id});
    if (res.status === 1) {
      Alert.alert('', 'Xoá sản phẩm thành công', [
        {
          text: 'Đồng ý',
          onPress: () => {
            getData();
          },
        },
      ]);
    } else {
      Alert.alert('', 'Xoá sản phẩm không thành công', []);
    }
  };

  const onDelete = () => {
    setModalVisible(false);
    Alert.alert('', 'Bạn có chắc muốn xoá sản phầm này?', [
      {
        text: 'Huỷ',
      },
      {
        onPress: handleDelete,
        text: 'Xoá',
      },
    ]);
  };

  const onEdit = () => {
    setModalVisible(false);
    console.log('bach currentItem', currentItem);
    refModalEditProduct.current.onShow(currentItem);
  };

  const RenderAddProduct = () => {
    return (
      <TouchableWithoutFeedback onPress={onPressAddNew}>
        <View style={styles.btAddProduct}>
          <Text style={styles.txtbtAddProduct}>Thêm sản phẩm</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => linkToShop(item?.link)}
        onLongPress={() => onLongPress(item)}
        style={styles.item}>
        <View style={styles.onSideItem}>
          <FastImage
            source={{
              uri: item.img,
            }}
            style={styles.imageProduct}
          />
          <Text numberOfLines={3} style={styles.nameProduct}>
            {item?.name}
          </Text>
          <Text style={styles.priceProduct}>₫ {item?.price}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.contain}>
      <SafeAreaView />
      <Header
        title={'Sản phẩm giới thiệu'}
        type={'iconLeft'}
        textColor={'#fff'}
        color={'#fff'}
      />
      {role === 'admin' && <RenderAddProduct />}
      <FlatList
        data={data}
        renderItem={renderItem}
        numColumns={2}
        ListFooterComponent={() => <View style={styles.footerComponent} />}
        contentContainerStyle={styles.list}
      />

      <ModalAddProduct reLoad={getData} ref={refModalAddProduct} />
      <ModalEditProduct reLoad={getData} ref={refModalEditProduct} />

      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible(false);
          }}>
          <View style={styles.contentModal}>
            <TouchableWithoutFeedback>
              <View style={styles.modal}>
                <TouchableWithoutFeedback onPress={onDelete}>
                  <View style={styles.btDelete}>
                    <Text style={styles.txtbtDelete}>Xoá sản phẩm</Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={onEdit}>
                  <View style={styles.btEdit}>
                    <Text style={styles.txtbtDelete}>Sửa sản phẩm</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  item: {
    width: '50%',
    paddingHorizontal: '2%',
    marginBottom: 10,
  },
  onSideItem: {
    width: '100%',
    backgroundColor: '#F3F3F3',
    borderRadius: 5,
    paddingBottom: 5,
  },
  footerComponent: {
    width: '100%',
    height: 60,
  },
  list: {
    width: '98%',
    alignSelf: 'center',
  },
  imageProduct: {
    width: width * 0.46 * 0.9,
    height: width * 0.46 * 0.9,
    alignSelf: 'center',
    marginTop: 5,
    alignSelf: 'center',
    borderRadius: 5,
  },
  nameProduct: {
    width: '90%',
    alignSelf: 'center',
    fontSize: 13,
    fontWeight: '500',
    color: '#000',
  },
  priceProduct: {
    fontSize: 13,
    fontWeight: '500',
    alignSelf: 'flex-end',
    right: '5%',
    color: '#ee4d2d',
    marginTop: 5,
  },
  btAddProduct: {
    width: '90%',
    height: 40,
    backgroundColor: '#5AC685',
    alignSelf: 'center',
    marginBottom: 10,
    borderRadius: 20,
    justifyContent: 'center',
  },
  txtbtAddProduct: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '700',
    color: '#fff',
  },
  contentModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
  },
  modal: {
    width: '90%',
    height: 80,
    alignSelf: 'center',
    backgroundColor: '#181c29',
    borderRadius: 10,
    paddingHorizontal: '5%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btDelete: {
    width: '45%',
    alignSelf: 'center',
    backgroundColor: '#c4c4c4',
    height: 40,
    justifyContent: 'center',
    borderRadius: 10,
  },
  btEdit: {
    width: '45%',
    alignSelf: 'center',
    backgroundColor: '#5AC685',
    height: 40,
    justifyContent: 'center',
    borderRadius: 10,
  },
  txtbtDelete: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
  },
});

export default Index;
