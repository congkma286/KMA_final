import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Header from '../../commons/Header';
import {getListChat} from '../../services/apiChat';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';

const Index = () => {
  const navigation = useNavigation();
  const [listChat, setListChat] = useState([]);
  const {_id} = useSelector(state => state.auth);

  const onPreesChat = item => {
    navigation.navigate('ContactAdmin', {userId: item?._id});
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableWithoutFeedback onPress={() => onPreesChat(item)}>
        <View style={styles.itemChat}>
          <Image
            source={require('../../assets/icons/iconApp.png')}
            style={styles.avt}
          />
          <Text style={styles.txtName}>{item?.name}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const getData = async () => {
    try {
      const data = await getListChat();
      if (data.status === 1) {
        const dataFil = data?.data.filter(a => a._id !== _id);
        setListChat(dataFil);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(async () => {
    getData();
  }, []);

  return (
    <View style={styles.contain}>
      <SafeAreaView />
      <Header
        title={'Kênh chat'}
        type={'iconLeft'}
        textColor={'#fff'}
        color={'#fff'}
      />
      <FlatList data={listChat} renderItem={renderItem} />
    </View>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  itemChat: {
    width: '100%',
    height: 70,
    marginTop: 10,
    borderBottomWidth: 1,
    paddingHorizontal: '5%',
    flexDirection: 'row',
    // justifyContent: 'space-between',
  },
  avt: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    borderRadius: 40,
  },
  txtName: {
    marginLeft: 8,
    fontSize: 15,
    fontWeight: '500',
    color: '#fff',
    alignSelf: 'center',
  },
});

export default Index;
