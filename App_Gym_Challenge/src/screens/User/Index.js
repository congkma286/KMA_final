import {
  faCartShopping,
  faClockRotateLeft,
  faGear,
  faHeart,
  faPhone,
  faRightFromBracket,
  faRotate,
  faStopwatch,
  faUser,
  faKey,
  faGlassWater,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  FlatList,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {changeInfo} from '../../redux/authSlice';
import {apiAddSync, apiSync, apiUpdateSync} from '../../services/apiSync';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  getDataExercise,
  getListKeyExercise,
  setKeyExercise,
} from '../../utils/strorage';
import {requestReload} from '../../redux/dataSlice';
import {setDataByKey} from '../../utils/strorage';

const dataLisstFuntions = [
  {
    title: 'Thông tin sức khoẻ',
    icon: faUser,
  },
  {
    title: 'Mục ưa thích của tôi',
    icon: faHeart,
  },
  {
    title: 'Uống nước',
    icon: faGlassWater,
  },
  {
    title: 'Nhắc nhở uống nước',
    icon: faStopwatch,
  },
  {
    title: 'Lịch sử',
    icon: faClockRotateLeft,
  },
  {
    title: 'Sản phẩm giới thiệu',
    icon: faCartShopping,
  },
  {
    title: 'Liên hệ chúng tôi',
    icon: faPhone,
  },
  {
    title: 'Liên hệ chatbot',
    icon: faPhone,
  },
];

const Index = () => {
  const navigation = useNavigation();
  const {name, _id, role} = useSelector(state => state.auth);
  const [listFunctions, setlistFunctions] = useState(dataLisstFuntions);
  const dispatch = useDispatch();

  const logout = async () => {
    try {
      await syncData(true);
      dispatch(
        changeInfo({
          nameUser: '',
          _id: '',
          name: '',
          role: '',
        }),
      );
      await AsyncStorage.setItem('@token', '');
      await AsyncStorage.setItem('@history', '[]');
      await AsyncStorage.setItem('@historyDrink', '[]');
      const listKey = await getListKeyExercise();
      listKey?.map(async item => {
        await AsyncStorage.setItem(item, '');
      });
      await AsyncStorage.setItem('@key_exercise', 'null');
      dispatch(requestReload());
      setTimeout(() => {
        dispatch(requestReload());
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  };

  const onPressFuntion = index => {
    switch (index) {
      case 0:
        navigation.navigate('Introduce');
        return;
      case 2:
        navigation.navigate('DrinkWater');
        return;
      case 1:
        navigation.navigate('LikeExercising');
        return;
      case 3:
        navigation.navigate('Warning');
        return;
      case 5:
        navigation.navigate('Product');
        return;
      case 6:
        if (_id) {
          if (role === 'user') {
            navigation.navigate('Contact');
            // navigation.navigate('ChatGPT');
          } else {
            navigation.navigate('ListContact');
          }
        } else {
          Alert.alert('', 'Bạn hãy đăng nhập để dùng chức năng này!');
        }
        return;
      case 7:
        if (_id) {
          navigation.navigate('ContactChatBot');
        } else {
          Alert.alert('', 'Bạn hãy đăng nhập để dùng chức năng này!');
        }
        return;
      case 8:
        navigation.navigate('ChangePassword');
        return;
      case 9:
        Alert.alert('', 'Bạn có muốn đăng xuất không', [
          {
            text: 'Huỷ',
          },
          {
            text: 'Đăng xuất',
            onPress: logout,
          },
        ]);
        return;
      default:
        break;
    }
  };

  useEffect(() => {
    if (_id && _id != '' && listFunctions.length === 8) {
      setlistFunctions([
        ...listFunctions,
        {
          title: 'Đổi mật khẩu',
          icon: faKey,
        },
        {
          title: 'Đăng xuất',
          icon: faRightFromBracket,
        },
      ]);
    } else {
      if (listFunctions.length === 10 && _id == '') {
        const a = listFunctions.slice(0, 8);
        setlistFunctions(a);
      }
    }
  }, [_id]);

  const filterDataDuplicateExercising = data => {
    if (data?.length > 0) {
      let dataOutput = [];
      data.map(item => {
        if (dataOutput.find(element => element.id == item.id)) {
          if (
            dataOutput.find(
              element => element.id == item.id && element.day < item.day,
            )
          ) {
            dataOutput[dataOutput.findIndex(a => a.id == item.id)] = item;
          }
        } else {
          dataOutput.push(item);
        }
      });
      return dataOutput;
    } else return [];
  };

  const filterDataDuplicateDrink = data => {
    if (data?.length > 0) {
      let dataOutput = [];
      data.map(item => {
        if (dataOutput.find(element => element.timeDay == item.timeDay)) {
          if (
            dataOutput.find(
              element =>
                element.timeDay == item.timeDay &&
                element.numDrink < item.numDrink,
            )
          ) {
            dataOutput[dataOutput.findIndex(a => a.timeDay == item.timeDay)] =
              item;
          }
        } else {
          dataOutput.push(item);
        }
      });
      return dataOutput;
    } else return [];
  };

  const filterDataDuplicateHistory = data => {
    if (data?.length > 0) {
      const dataOutput = [];
      data.map(item => {
        if (
          dataOutput.find(
            element =>
              element.timeDay === item.timeDay &&
              element.id === item.id &&
              element.day === item.day,
          )
        ) {
        } else {
          dataOutput.push(item);
        }
      });
      return dataOutput;
    } else return [];
  };

  // Đồng bộ dữ liệu
  // Các dữ liệu được đồng bộ: lịch sử tập, lịch sử uống nước, các bài tập đang tập
  // khi đồng bộ dữ liệu, sẽ lấy dữ liệu từ trên database xuống rồi kết hợp với data dưới máy rồi cập nhật lại lên database
  // Data khi trùng nhau sẽ ưu tiên lấy data dưới máy (vì người dùng tập luyện tiếp sẽ sinh ra data nên data trên database sẽ bị cũ hơn)

  const syncData = async (isLogout = false) => {
    try {
      console.log('bách isLogout', isLogout);
      const token = await AsyncStorage.getItem('@token');
      const res = await apiSync({token});
      if (res?.status === 1) {
        const dataApi = res.data;
        console.log('dataApi', dataApi);
        let data = {};
        if (dataApi?.length == 1) {
          if (dataApi[0]?.data) {
            data = JSON.parse(dataApi[0]?.data);
          }
        }

        const listKey = await getListKeyExercise();
        const listKeyJson = (await getDataExercise(listKey)) || [];
        let dataExercisingApi = data?.dataExercising || [];
        const dataExercising = filterDataDuplicateExercising([
          ...listKeyJson,
          ...dataExercisingApi,
        ]);

        const history = await AsyncStorage.getItem('@history');
        const historyJson = JSON.parse(history) || [];
        let dataHistoryApi = data?.dataHistory || [];
        const dataHistory = filterDataDuplicateHistory([
          ...historyJson,
          ...dataHistoryApi,
        ]);

        const dataStrogeDrink = await AsyncStorage.getItem('@historyDrink');
        const dataHistoryJson = JSON.parse(dataStrogeDrink) || [];
        let dataDrinkApi = data?.dataDrink || [];
        const dataDrink = filterDataDuplicateDrink([
          ...dataHistoryJson,
          ...dataDrinkApi,
        ]);

        const body = {
          data: {
            dataHistory: dataHistory,
            dataExercising: dataExercising,
            dataDrink: dataDrink,
          },
        };

        console.log('bách body', body);

        if (data?.dataHistory || data?.dataDrink || data?.dataExercising) {
          const resUpdate = await apiUpdateSync({token, body});
          if (!isLogout) {
            Alert.alert('', 'Thành công');
          }
        } else {
          const resAdd = await apiAddSync({token, body});
          if (!isLogout) {
            Alert.alert('', 'Thành công');
          }
        }
        if (!isLogout) {
          if (dataExercising) {
            const listCha = [];
            dataExercising.map(item => {
              listCha.push(item.id);
              setDataByKey(item.id, item);
            });
            setKeyExercise(listCha);
          }
          if (dataHistory) {
            await AsyncStorage.setItem('@history', JSON.stringify(dataHistory));
          }
          if (dataDrink) {
            await AsyncStorage.setItem(
              '@historyDrink',
              JSON.stringify(dataDrink),
            );
          }
          dispatch(requestReload());
        }
      } else {
        Alert.alert('', 'Đã có lỗi xảy ra!');
      }
    } catch (error) {
      console.log('error sync', error);
    }
  };

  const onPressSync = () => {
    if (name) {
      syncData();
    } else {
      navigation.navigate('Login');
    }
  };

  const Header = () => {
    return (
      <View>
        <FastImage
          style={styles.avatar}
          source={require('../../assets/images/image-avatar-default.png')}
        />

        {name ? (
          <Text style={styles.txtGreeting}>Xin chào {name}</Text>
        ) : (
          <Text style={styles.txtGreeting}>Xin chào mừng bạn!</Text>
        )}

        <TouchableWithoutFeedback onPress={onPressSync}>
          <View style={styles.btSynch}>
            <Text style={styles.txtBtSynch}>
              {name ? 'Sao Lưu & Khôi Phục' : 'Đăng nhập'}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <View style={[styles.row, styles.rowLogin]}>
          <FontAwesomeIcon color={'#5AC685'} size={16} icon={faRotate} />
          <Text style={styles.txtLoginSynch}>
            Đăng nhập và đồng bộ dữ liệu của bạn
          </Text>
        </View>
      </View>
    );
  };

  const renderItemFunction = ({item, index}) => {
    return index === 4 ? (
      <></>
    ) : (
      <TouchableWithoutFeedback onPress={() => onPressFuntion(index)}>
        <View style={styles.itemFunction}>
          <View style={styles.row}>
            <FontAwesomeIcon
              style={styles.iconItemFunction}
              color={'#5AC685'}
              size={26}
              icon={item.icon}
            />
            <Text style={styles.titleItemFunction}>{item.title}</Text>
          </View>
          <FastImage
            style={styles.iconArrowRight}
            tintColor={'#444851'}
            source={require('../../assets/icons/icon_arrow_left.png')}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.container}>
      <SafeAreaView />
      <Header />
      <FlatList
        data={listFunctions}
        renderItem={renderItemFunction}
        keyExtractor={(_, index) => JSON.stringify(index)}
        style={styles.listFunctions}
      />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  avatar: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    borderRadius: 100,
    marginTop: 20,
  },
  txtGreeting: {
    alignSelf: 'center',
    marginTop: 10,
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
  },
  btSynch: {
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 30,
    backgroundColor: '#5AC685',
    height: 46,
    paddingHorizontal: '10%',
    marginTop: 15,
  },
  txtBtSynch: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
  },
  row: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  rowLogin: {
    marginTop: 10,
  },
  txtLoginSynch: {
    fontSize: 14,
    color: '#fff',
    marginLeft: 5,
  },
  listFunctions: {
    marginTop: 40,
  },
  itemFunction: {
    width: '92%',
    alignSelf: 'center',
    height: 50,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconItemFunction: {
    alignSelf: 'center',
  },
  titleItemFunction: {
    alignSelf: 'center',
    fontWeight: '600',
    fontSize: 17,
    color: '#fff',
    marginLeft: 14,
  },
  iconArrowRight: {
    alignSelf: 'center',
    width: 24,
    height: 24,
    transform: [{rotate: '180deg'}],
  },
});

export default Index;
