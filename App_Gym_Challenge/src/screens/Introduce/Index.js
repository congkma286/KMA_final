/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  Text,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  ImageBackground,
  Dimensions,
} from 'react-native';
import Header from '../../commons/Header';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalBMI from '../../components/ModalBMI';
import TextAnimator from '../../components/TextAnimator';
import BMI from '../../components/BMIMale';
import Toast from 'react-native-easy-toast';
const heightDe = Dimensions.get('window').height;

const Index = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isCanSave, setIsCanSave] = useState(false);

  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [gender, setGender] = useState('');
  const [textInput, setTextInput] = useState('');
  const [typeModal, setTypeModal] = useState();

  const [typeBMI, setTypeBMI] = useState('');

  const refModalBMI = useRef();
  const refToast = useRef(null);

  const getData = async () => {
    const value = await AsyncStorage.getItem('@parameterUser');
    console.log('value', value);
    if (value) {
      const {height, weigh, gender} = JSON.parse(value);
      setHeight(height);
      setWeight(weigh);
      setGender(gender);
    }
    setIsCanSave(true);
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (weight > 0 && height > 0) {
      setTypeBMI(BMI(weight, height));
    } else {
      setTypeBMI('');
    }
  }, [height, weight]);

  const save = useCallback(async () => {
    try {
      const value = JSON.stringify({height, weigh: weight, gender});
      await AsyncStorage.setItem('@parameterUser', value);
    } catch (e) {}
  }, [gender, height, weight]);

  useEffect(() => {
    if (isCanSave) {
      save();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [height, weight, gender, save]);

  const onChangeGender = () => {
    if (gender === 'NAM') {
      setGender('NU');
    } else {
      setGender('NAM');
    }
  };

  const editHeight = () => {
    setTextInput(height);
    setModalVisible(true);
    setTypeModal('height');
  };

  const editWeight = () => {
    setTextInput(weight);
    setModalVisible(true);
    setTypeModal('weight');
  };

  const onCancel = () => {
    setModalVisible(false);
    setTextInput('');
  };

  const onConfirm = () => {
    if (typeModal === 'height') {
      if (Number(textInput) < 140) {
        refToast.current.show('Chiều cao phải lớn hơn 140', 2000);
        return;
      }
      if (Number(textInput) > 200) {
        refToast.current.show('Chiều cao phải nhỏ hơn 200', 2000);
        return;
      }

      setHeight(textInput);
    } else {
      setWeight(textInput);
    }
    onCancel();
  };

  const onPressBA = () => {
    refModalBMI.current.onShow({gender, height, weight});
  };

  const RenderTextBMI = useCallback(() => {
    return typeBMI ? (
      <TextAnimator
        content={typeBMI}
        textStyle={styles.textStyle}
        style={styles.containerStyle}
        duration={1000}
        onFinish={() => {}}
      />
    ) : (
      <></>
    );
  }, [typeBMI]);

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.container}>
      <SafeAreaView />
      <Header
        type={'iconLeft'}
        title={'Thông tin sức khoẻ'}
        color={'#fff'}
        textColor={'#fff'}
      />
      <TouchableWithoutFeedback onPress={editHeight}>
        <View style={[styles.row, styles.top]}>
          <Text style={styles.title}>Chiều cao</Text>
          <Text style={styles.txtValue}>{height} cm</Text>
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.line} />
      <TouchableWithoutFeedback onPress={editWeight}>
        <View style={styles.row}>
          <Text style={styles.title}>Cân nặng</Text>
          <Text style={styles.txtValue}>{weight} kg</Text>
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.line} />
      <View style={styles.row}>
        <Text style={styles.title}>Giới tính</Text>
        <TouchableWithoutFeedback onPress={onChangeGender}>
          <View style={styles.swiper}>
            <View
              style={[
                styles.barkGender,
                {backgroundColor: gender === 'NAM' ? '#5AC685' : '#303545'},
              ]}>
              <Text style={styles.txtGender}>Nam</Text>
            </View>

            <View
              style={[
                styles.barkGender,
                {backgroundColor: gender === 'NU' ? '#5AC685' : '#303545'},
              ]}>
              <Text style={styles.txtGender}>Nữ</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>

      <RenderTextBMI />

      <TouchableWithoutFeedback onPress={onPressBA}>
        <View style={styles.btCheck}>
          <Text style={styles.txttx}>Xem tỉ lệ cân nặng</Text>
        </View>
      </TouchableWithoutFeedback>

      <ModalBMI ref={refModalBMI} />

      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TextInput
              style={styles.input}
              value={textInput}
              onChangeText={setTextInput}
              keyboardType={'numeric'}
            />
            <View style={styles.listButtonModal}>
              <TouchableWithoutFeedback onPress={onCancel}>
                <View style={styles.btCancel}>
                  <Text style={styles.txtCancel}>Huỷ</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={onConfirm}>
                <View style={styles.btConfirm}>
                  <Text style={styles.txtConfirm}>Xác nhận</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>

          <Toast position="top" ref={refToast} />
        </View>
      </Modal>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: '90%',
    justifyContent: 'space-between',
  },
  top: {
    marginTop: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
    alignSelf: 'center',
  },
  txtValue: {
    fontSize: 16,
    fontWeight: '600',
    color: '#5AC685',
    alignSelf: 'center',
  },
  line: {
    width: '90%',
    alignSelf: 'center',
    height: 2,
    backgroundColor: '#2E3137',
    marginVertical: 14,
  },
  swiper: {
    width: '40%',
    height: 36,
    backgroundColor: '#303545',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  barkGender: {
    flex: 1,
    justifyContent: 'center',
    borderRadius: 10,
  },
  txtGender: {
    alignSelf: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: '#fff',
  },
  centeredView: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
  },
  modalView: {
    alignSelf: 'center',
    width: '90%',
    height: 130,
    backgroundColor: '#303545',
    borderRadius: 10,
    justifyContent: 'center',
  },
  input: {
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    borderWidth: 1,
    height: 40,
    borderColor: '#fff',
    color: '#fff',
    paddingHorizontal: '3%',
    fontWeight: '400',
    fontSize: 16,
  },
  listButtonModal: {
    alignSelf: 'center',
    width: '90%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 15,
  },
  btCancel: {
    width: '40%',
    alignSelf: 'center',
    backgroundColor: '#c4c4c4',
    height: 35,
    justifyContent: 'center',
    borderRadius: 10,
  },
  txtCancel: {
    alignSelf: 'center',
    fontWeight: '500',
    fontSize: 16,
    color: '#000',
  },
  btConfirm: {
    width: '40%',
    alignSelf: 'center',
    backgroundColor: '#5AC685',
    height: 35,
    justifyContent: 'center',
    borderRadius: 10,
  },
  txtConfirm: {
    alignSelf: 'center',
    fontWeight: '600',
    fontSize: 16,
    color: '#fff',
  },
  btCheck: {
    width: '60%',
    height: 40,
    backgroundColor: '#5AC685',
    alignSelf: 'center',
    borderRadius: 40,
    justifyContent: 'center',
    position: 'absolute',
    top: heightDe - 100,
  },
  txttx: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '600',
    color: '#fff',
  },
  textStyle: {
    fontWeight: '500',
    fontSize: 20,
    color: '#5AC685',
    lineHeight: 28,
  },
  containerStyle: {
    width: '86%',
    alignSelf: 'center',
    textAlign: 'center',
    marginTop: 40,
  },
});

export default Index;
