import {
  View,
  Text,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Header from '../../commons/Header';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import {requestReload} from '../../redux/dataSlice';

let clickCount = 0;
let timeoutId = null;

const Index = () => {
  const INITIAL_DATE = moment(now).format('YYYY-MM-DD');
  const now = new Date();
  const [numDrink, setNumDrink] = useState(0);

  const dispatch = useDispatch();

  const [historyDrink, setDataHistoryDrink] = useState([]);

  const onPressDrink = async () => {
    clickCount++;
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    
    timeoutId = setTimeout(() => {
      clickCount = 0;
      timeoutId = null;
    }, 30000);

    if (clickCount > 2) {
      Alert.alert('', 'Bạn không nên uống quá nhiều nước một lúc');
      return;
    }


    setNumDrink(numDrink + 1);

    const idDay = moment(now).format('DD/MM/YYYY');

    const data = {
      timeDay: idDay,
      numDrink: numDrink + 1,
    };
    const dataSave = historyDrink.filter(a => a.timeDay !== idDay);
    dataSave.push(data);
    await AsyncStorage.setItem('@historyDrink', JSON.stringify(dataSave));
    dispatch(requestReload());
  };

  useEffect(async () => {
    const dataStrogeDrink = await AsyncStorage.getItem('@historyDrink');
    if (dataStrogeDrink !== null) {
      const data2 = JSON.parse(dataStrogeDrink);
      setDataHistoryDrink(data2);

      const idDay = moment(now).format('DD/MM/YYYY');
      const dataH2 = data2.filter(a => a.timeDay === idDay);
      if (dataH2.length > 0) {
        setNumDrink(dataH2[0].numDrink);
      } else {
        setNumDrink(0);
      }
    }
  }, []);

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.contain}>
      <SafeAreaView />
      <Header
        title={'Uống nước'}
        type={'iconLeft'}
        textColor={'#fff'}
        color={'#fff'}
      />
      <Text style={styles.title}>Lượng nước bạn đã uống trong ngày</Text>
      <Text style={styles.txtNumDrink}>{numDrink}</Text>

      <TouchableWithoutFeedback onPress={onPressDrink}>
        <View style={styles.btDrink}>
          <Text style={styles.txtbt}>Hãy uống 1 cốc nước</Text>
        </View>
      </TouchableWithoutFeedback>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  title: {
    alignSelf: 'center',
    marginTop: 100,
    fontSize: 18,
    fontWeight: '700',
    color: '#5AC685',
    textAlign: 'center',
  },
  txtNumDrink: {
    alignSelf: 'center',
    marginTop: 40,
    fontSize: 70,
    fontWeight: '700',
    color: '#5AC685',
  },
  btDrink: {
    width: '60%',
    height: 40,
    backgroundColor: '#5AC685',
    alignSelf: 'center',
    marginTop: 30,
    borderRadius: 20,
    justifyContent: 'center',
  },
  txtbt: {
    alignSelf: 'center',
    fontWeight: '600',
    fontSize: 18,
    color: '#fff',
  },
});

export default Index;
