import {
  View,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  Text,
  TouchableWithoutFeedback,
  ImageBackground,
} from 'react-native';
import React, {useRef, useState, useCallback, useEffect} from 'react';
import Header from '../../commons/Header';
import YoutubePlayer from 'react-native-youtube-iframe';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';
import ModalCountDown from '../../components/ModalCountDown';
import {useRoute} from '@react-navigation/native';
import ModalPrepare from '../../components/ModalPrepare';
import ModalComplete from '../../components/ModalComplete';
import ModalWarning from '../../components/ModalWarning';

const {width} = Dimensions.get('window');

const getDuration = data => {
  if (data.time !== 0) {
    return data.time;
  } else {
    return data.rep * 3;
  }
};

const Index = (props) => {
  const playerRef = useRef(null);
  const refModalCountDown = useRef(null);
  const refModalPrepare = useRef(null);
  const refModalComplete = useRef(null);
  const refModalWarning = useRef(null);

  // const route = useRoute();
  const route = props?.route;
  const {data, parent} = route.params;
  const [playing, setPlaying] = useState(false);
  const [isPlayingCountDown, setIsPlayingCountDown] = useState(false);
  const [isShowCountdownCircleTimer, setIsShowCountdownCircleTimer] =
    useState(true);
  const [duration, setDuration] = useState(getDuration(data[0]));

  const [numericalOrder, setNumericalOrder] = useState(0);
  const [idVideo, setIdVideo] = useState(data[0].id);

  const countdownPress = useRef(null);

  /*
    0 trạng thái chuẩn bị bài tập
    1 trạng thái đang tập
    2 trạng thái tạm dừng tập
  */
  const [status, setStatus] = useState(0);

  useEffect(() => {
    onReady();
    countdownPress.current = setTimeout(() => {
      refModalWarning.current.onShow();
    }, 60000);

    return () => {
      clearTimeout(countdownPress.current);
    };
  }, []);

  const resetCheckWarning = () => {
    if (countdownPress.current) {
      clearTimeout(countdownPress.current);
    }
    countdownPress.current = setTimeout(() => {
      refModalWarning.current.onShow();
    }, 60000);
  };

  const onPressResetWarning = () => {
    refModalWarning.current.onHide();
    resetCheckWarning();
  };

  const setAgainDuration = time => {
    setDuration(time);
    setIsShowCountdownCircleTimer(false);
    setTimeout(() => {
      setIsShowCountdownCircleTimer(true);
    }, 100);
  };

  const onReady = () => {
    setTimeout(() => {
      setPlaying(true);
    }, 500);
    setAgainDuration(10);
    setIsPlayingCountDown(true);
    setStatus(0);
  };

  const onStateChange = useCallback(state => {
    if (state === 'ended') {
      setPlaying(false);
    }
  }, []);

  const onUpdate = remainingTime => {
    console.log('remainingTime', remainingTime);
  };

  const handleStart = () => {
    setAgainDuration(getDuration(data[numericalOrder]));
    setStatus(1);
  };

  const handleNext = () => {
    resetCheckWarning();
    onReady();
    // refModalCountDown.current.onShow(3);
    // setIsPlayingCountDown(true);
    // setPlaying(true);

    setNumericalOrder(numericalOrder + 1);
    setIdVideo(data[numericalOrder + 1].id);
  };

  const startCountDown = () => {
    setIsPlayingCountDown(true);
    setPlaying(true);
  };

  const onPause = () => {
    if (isPlayingCountDown) {
      setPlaying(false);
      setIsPlayingCountDown(false);
      setStatus(2);
    } else {
      setPlaying(true);
      setIsPlayingCountDown(true);
      setStatus(1);
    }
  };

  const onComplete = () => {
    switch (status) {
      case -1:
        // sau khi tập xong 1 bài
        // onReady();
        break;
      case 0:
        // sau khi chuẩn bị
        // handleStart();
        break;
      case 1:
        // sau khi tập xong
        if (numericalOrder === data?.length - 1) {
          setIsPlayingCountDown(false);
          setPlaying(false);
          refModalComplete.current.onShow();
          clearTimeout(countdownPress.current);
        } else {
          refModalPrepare.current.onShow(data[numericalOrder + 1]);
          setIsPlayingCountDown(false);
          setPlaying(false);
          setStatus(0);
          clearTimeout(countdownPress.current);
        }
        break;
      default:
        break;
    }

    // if (numericalOrder < data.length - 1) {
    //   refModalPrepare.current.onShow(data[numericalOrder + 1]);
    // } else {

    // }
  };

  const onPressWithStatus = () => {
    resetCheckWarning();
    switch (status) {
      case 0:
        // đang chuẩn bị -> bắt đầu tập
        handleStart();
        break;
      case 1:
        // đang tập -> tạm dừng
        onPause();
        break;
      case 2:
        // tạm dừng -> tiếp tục
        onPause();
        break;
      default:
        return '';
    }
  };

  const getTextButton = key => {
    //isPlayingCountDown ? 'Tạm dừng' : 'Tiếp tục'
    switch (key) {
      case 0:
        return 'Bắt đầu';
      case 1:
        return 'Tạm dừng';
      case 2:
        return 'Tiếp tục';
      default:
        return '';
    }
  };

  return (
    <ImageBackground source={require('../../assets/images/hinhnen2.png')} style={styles.container}>
      <StatusBar barStyle={'light-content'} />
      <SafeAreaView />
      <Header type={'iconLeft'} title={``} color={'#fff'} textColor={'#fff'} />
      <YoutubePlayer
        ref={playerRef}
        height={(9 * width) / 16}
        play={playing}
        videoId={idVideo}
        onChangeState={onStateChange}
      />
      <Text style={styles.txtReady}>ĐÃ SẴN SÀNG TẬP!</Text>
      <Text style={styles.txtName}>{data[numericalOrder]?.title}</Text>
      <View style={styles.blockCountDown}>
        {isShowCountdownCircleTimer && (
          <CountdownCircleTimer
            isPlaying={isPlayingCountDown}
            duration={duration}
            size={100}
            onUpdate={onUpdate}
            onComplete={onComplete}
            // updateInterval={5}
            colors={'#5AC685'}
            colorsTime={[7, 5, 2, 0]}>
            {({remainingTime}) => (
              <Text style={styles.txtNumCountDown}>{remainingTime}</Text>
            )}
          </CountdownCircleTimer>
        )}
      </View>
      <TouchableWithoutFeedback onPress={onPressWithStatus}>
        <View style={styles.btPause}>
          <Text style={styles.txtBtPause}>{getTextButton(status)}</Text>
        </View>
      </TouchableWithoutFeedback>

      <Text style={styles.txtDep}>{data[numericalOrder]?.dep}</Text>

      <ModalCountDown ref={refModalCountDown} handleAfter={startCountDown} />
      <ModalPrepare ref={refModalPrepare} handleAfter={handleNext} />
      <ModalComplete data={data} parent={parent} ref={refModalComplete} />
      <ModalWarning ref={refModalWarning} onPress={onPressResetWarning} />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  txtReady: {
    fontSize: 22,
    alignSelf: 'center',
    marginTop: 20,
    color: '#5AC685',
    fontWeight: '700',
    fontFamily: 'Nunito',
  },
  txtName: {
    fontSize: 19,
    alignSelf: 'center',
    marginTop: 10,
    color: '#fff',
    fontWeight: '600',
    fontFamily: 'Nunito',
  },
  blockCountDown: {
    alignSelf: 'center',
    marginTop: 40,
  },
  txtNumCountDown: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '700',
    color: '#5AC685',
  },
  btPause: {
    width: width / 2,
    height: 50,
    backgroundColor: '#77C38A',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: 30,
  },
  txtBtPause: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '700',
    color: '#fff',
  },
  txtDep: {
    width: '90%',
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    marginTop: 20,
    fontWeight: '500',
  },
});

export default Index;
