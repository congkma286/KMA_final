import {faStar} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useRef} from 'react';
import {
  Alert,
  Dimensions,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import PushNotification from 'react-native-push-notification';
import {useDispatch, useSelector} from 'react-redux';
import Progress from '../../components/Progress';
import {exerciseFormat, getImgById} from '../../constants/exerciseFormat';
import {
  setListIdChallenge,
  setMyChallenge,
  setOtherChallenge,
} from '../../redux/dataSlice';
import {
  getDataExercise,
  getListKeyExercise,
  getWeigh,
} from '../../utils/strorage';
import ModalWeigh from '../../components/ModalWeigh';

const {width} = Dimensions.get('window');

const Index = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {myChallenge, otherChallenge, reload} = useSelector(
    state => state.data,
  );
  const {_id} = useSelector(state => state.auth);

  const refModalWeigh = useRef(null);

  useEffect(() => {
    PushNotification.configure({
      onRegister: function (token) {
        console.log('TOKEN:', token);
      },
      onNotification: function (notification) {
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  }, [dispatch]);

  const onPressItem = item => {
    if (!_id) {
      Alert.alert('', 'Hãy đăng nhập để có trải nghiệm tốt nhất', [
        {
          text: 'Bỏ qua',
          onPress: () => changeScr(item),
        },
        {
          text: 'Đăng nhập',
          onPress: () => navigation.navigate('Login'),
        },
      ]);
    } else {
      changeScr(item);
    }
  };

  const changeScr = item => {
    if (item.level) {
      console.log('item', item);
      navigation.navigate('Daily', {data: item, level: item.level});
    } else {
      navigation.navigate('Level', {item});
    }
  };

  const getData = async () => {
    const listKey = await getListKeyExercise();
    const dataStrorage = await getDataExercise(listKey);
    dispatch(setListIdChallenge(listKey));
    dispatch(setMyChallenge(dataStrorage));
    const dataOther = exerciseFormat.filter(i => !listKey?.includes(i.id));
    dispatch(setOtherChallenge(dataOther));
  };

  useEffect(() => {
    getData();
  }, [reload]);

  const checkWeigh = async () => {
    const dataWeigh = await getWeigh();
    if (dataWeigh?.gender) {
    } else {
      refModalWeigh.current.onShow();
    }
  };

  useEffect(() => {
    checkWeigh();
  }, []);

  const headerList = text => {
    return <Text style={styles.title}>{text}</Text>;
  };

  const renderItem = ({item}) => {
    return (
      <TouchableWithoutFeedback onPress={() => onPressItem(item)}>
        <ImageBackground
          style={styles.bannerRender}
          source={getImgById(item?.id)}>
          <View style={styles.itemRender}>
            <Text style={styles.txtName}>{item?.title}</Text>
            {item?.progress ? (
              <View style={styles.tabProgress}>
                <Progress width={width * 0.8} progress={item?.progress} />
                <Text style={styles.txtProgress}>{item?.progress}%</Text>
              </View>
            ) : (
              <></>
            )}
          </View>
        </ImageBackground>
      </TouchableWithoutFeedback>
    );
  };
  console.log('myChallenge', myChallenge);
  return (
    <ImageBackground
      style={styles.container}
      source={require('../../assets/images/hinhnen2.png')}>
      <StatusBar barStyle={'light-content'} />
      <SafeAreaView />
      <View style={styles.rowTitle}>
        <Text style={styles.titleHome}>THỬ THÁCH TẬP 30 NGÀY</Text>
        <FontAwesomeIcon color={'#ffff'} size={22} icon={faStar} />
      </View>
      <ScrollView>
        {myChallenge.length > 0 ? (
          <>
            {headerList('THỬ THÁCH CỦA TÔI')}
            {myChallenge.map(item => renderItem({item}))}
          </>
        ) : (
          <></>
        )}
        <View style={styles.space} />
        {otherChallenge.length > 0 ? (
          <>
            {headerList('TẤT CẢ CÁC THỬ THÁCH')}
            {otherChallenge.map(item => renderItem({item}))}
          </>
        ) : (
          <></>
        )}
      </ScrollView>

      <ModalWeigh ref={refModalWeigh} />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  rowTitle: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  titleHome: {
    fontSize: 20,
    fontWeight: '800',
    fontFamily: 'Nunito-Bold',
    color: '#fff',
    marginBottom: 30,
  },
  title: {
    fontSize: 15,
    fontWeight: '800',
    fontFamily: 'Nunito-Bold',
    color: '#fff',
    width: '90%',
    alignSelf: 'center',
    marginTop: 10,
  },
  bannerRender: {
    width: width * 0.9,
    alignSelf: 'center',
    height: 100,
    marginTop: 10,
    borderRadius: 3,
    backgroundColor: '#fff',
  },
  itemRender: {
    width: '100%',
    alignSelf: 'center',
    height: 100,
    borderRadius: 3,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  txtName: {
    fontSize: 16,
    fontWeight: '800',
    color: '#fff',
    top: '50%',
    left: '5%',
    fontFamily: 'Nunito',
    transform: [{translateY: -7}],
  },
  tabProgress: {
    top: '50%',
  },
  txtProgress: {
    marginTop: 3,
    fontSize: 14,
    fontWeight: '700',
    color: '#fff',
    left: '5%',
  },
  space: {
    width: '100%',
    height: 30,
  },
});

export default Index;
