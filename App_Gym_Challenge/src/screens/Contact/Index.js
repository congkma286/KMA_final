import {faPaperPlane} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React, {useCallback, useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {GiftedChat, Send} from 'react-native-gifted-chat';
import Header from '../../commons/Header';
import firestore from '@react-native-firebase/firestore';
import {useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {requestGPT} from '../../services/apiChat';

const Index = () => {
  const {name, _id} = useSelector(state => state.auth);
  const idMess = _id;
  const [messages, setMessages] = useState([
    {
      _id: 1,
      text: 'Xin chào! Chúng tôi có thể hỗ trợ gì cho bạn?',
      createdAt: new Date(),
      user: {
        _id: 2,
        name: 'Support',
      },
    },
  ]);

  useEffect(() => {
    try {
      firestore()
        .collection(idMess)
        .orderBy('createdAt', 'desc')
        .onSnapshot(documentSnapshot => {
          const dataMess = [];
          documentSnapshot._docs.forEach(doc => {
            dataMess.push(doc.data());
          });
          if (dataMess.length == 0) {
          } else {
            setMessages(dataMess);
          }
        });
    } catch (error) {}
  }, []);

  const sendGpt = async messages => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await requestGPT({token, prompt: messages});
      console.log('res', res);
    } catch (error) {}
  };

  const onSend = useCallback((messages = []) => {
    // sendGpt(messages[0].text);
    firestore()
      .collection(idMess)
      .add({
        _id: messages[0]._id,
        text: messages[0].text,
        createdAt: new Date().getTime(),
        user: {
          _id: messages[0].user._id,
          name: messages[0].user.name,
        },
      })
      .then(() => {
        console.log('mess sended!');
      });
  }, []);

  return (
    <View style={styles.container}>
      <SafeAreaView />
      <Header
        title={'Liên hệ với chúng tôi'}
        type={'iconLeft'}
        textColor={'#fff'}
        color={'#fff'}
      />
      <GiftedChat
        messages={messages}
        onSend={messages => onSend(messages)}
        user={{
          _id: 1,
          name: name,
        }}
        renderSend={props => (
          <Send {...props}>
            <FontAwesomeIcon
              style={styles.iconSend}
              color={'#5AC685'}
              size={24}
              icon={faPaperPlane}
            />
          </Send>
        )}
        renderAvatar={() => (
          <FastImage
            style={styles.avatar}
            source={require('../../assets/icons/iconApp.png')}
          />
        )}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  iconSend: {
    right: 10,
    top: -10,
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 30,
    alignSelf: 'center',
  },
});

export default Index;
