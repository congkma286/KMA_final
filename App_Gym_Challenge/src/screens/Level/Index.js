import {faCircleInfo, faMedal} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Dimensions,
  Modal,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Header from '../../commons/Header';
import {setDataByKey, setKeyExercise} from '../../utils/strorage';
import {useDispatch, useSelector} from 'react-redux';
import {requestReload} from '../../redux/dataSlice';
import {ImageBackground} from 'react-native';

const {width} = Dimensions.get('window');

const Index = props => {
  const navigation = useNavigation();
  // const route = useRoute();
  const route = props?.route;

  const {listIdChallenge} = useSelector(state => state.data);
  const {item} = route.params;
  const [isVisibleModalInfo, setIsVisibleModalInfo] = useState(false);
  const dispatch = useDispatch();

  const onPressIconHeader = () => {
    setIsVisibleModalInfo(true);
  };

  const hideModalInfo = () => {
    setIsVisibleModalInfo(false);
  };

  const onPressLevelItem = async lv => {
    try {
      let itemChange = {...item};
      itemChange.level = lv.level;
      itemChange.day = 1;
      setDataByKey(item.id, itemChange);
      setKeyExercise([...listIdChallenge, item.id]);
      navigation.navigate('Daily', {data: itemChange, level: lv.level});
      dispatch(requestReload());
    } catch (error) {
      console.log(error);
    }
  };

  const renderIconRight = () => {
    return <FontAwesomeIcon color={'#fff'} size={22} icon={faCircleInfo} />;
  };

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.container}>
      <StatusBar barStyle={'light-content'} />
      <SafeAreaView />
      <Header
        type={'full'}
        title={'Mức độ'}
        iconRight={renderIconRight}
        color={'#fff'}
        textColor={'#fff'}
        onPressRight={onPressIconHeader}
      />
      <TouchableWithoutFeedback
        onPress={() => onPressLevelItem({level: 1, title: 'Dễ'})}>
        <View style={styles.rowRank}>
          <FontAwesomeIcon color={'#E17540'} size={22} icon={faMedal} />
          <Text style={styles.txtRank}>Dễ</Text>
        </View>
      </TouchableWithoutFeedback>
      {/* <TouchableWithoutFeedback
        onPress={() => onPressLevelItem({level: 2, title: 'Dễ 2'})}>
        <View style={styles.rowRank}>
          <FontAwesomeIcon color={'#E17540'} size={22} icon={faMedal} />
          <Text style={styles.txtRank}>Dễ 2</Text>
        </View>
      </TouchableWithoutFeedback> */}
      <View style={styles.boundary} />
      <TouchableWithoutFeedback
        onPress={() => onPressLevelItem({level: 2, title: 'Trung bình'})}>
        <View style={styles.rowRank}>
          <FontAwesomeIcon color={'#C3C6CD'} size={22} icon={faMedal} />
          <Text style={styles.txtRank}>Trung bình</Text>
        </View>
      </TouchableWithoutFeedback>
      {/* <TouchableWithoutFeedback
        onPress={() => onPressLevelItem({level: 4, title: 'Trung bình 2'})}>
        <View style={styles.rowRank}>
          <FontAwesomeIcon color={'#C3C6CD'} size={22} icon={faMedal} />
          <Text style={styles.txtRank}>Trung bình 2</Text>
        </View>
      </TouchableWithoutFeedback> */}
      <View style={styles.boundary} />
      <TouchableWithoutFeedback
        onPress={() => onPressLevelItem({level: 3, title: 'Khó'})}>
        <View style={styles.rowRank}>
          <FontAwesomeIcon color={'#EFD05C'} size={22} icon={faMedal} />
          <Text style={styles.txtRank}>Khó</Text>
        </View>
      </TouchableWithoutFeedback>
      {/* <TouchableWithoutFeedback
        onPress={() => onPressLevelItem({level: 6, title: 'Khó 2'})}>
        <View style={styles.rowRank}>
          <FontAwesomeIcon color={'#EFD05C'} size={22} icon={faMedal} />
          <Text style={styles.txtRank}>Khó 2</Text>
        </View>
      </TouchableWithoutFeedback> */}

      <Modal transparent visible={isVisibleModalInfo}>
        <TouchableWithoutFeedback onPress={hideModalInfo}>
          <View style={styles.containModal}>
            <TouchableWithoutFeedback>
              <View style={styles.viewInfo}>
                <View style={styles.rowRank}>
                  <View style={styles.leftRowRank}>
                    <FontAwesomeIcon
                      color={'#E17540'}
                      size={22}
                      icon={faMedal}
                    />
                  </View>
                  <Text style={styles.descriptionRank}>
                    Kế hoạch tập luyện dễ dàng được thiết kế cho những người tập
                    thể dục 1 lần một tuần
                  </Text>
                </View>
                <View style={styles.rowRank}>
                  <View style={styles.leftRowRank}>
                    <FontAwesomeIcon
                      color={'#C3C6CD'}
                      size={22}
                      icon={faMedal}
                    />
                  </View>
                  <Text style={styles.descriptionRank}>
                    Kế hoạch tập luyện trung bình được thiết kế cho những người
                    tập từ 2-3 lần một tuần.
                  </Text>
                </View>
                <View style={styles.rowRank}>
                  <View style={styles.leftRowRank}>
                    <FontAwesomeIcon
                      color={'#EFD05C'}
                      size={22}
                      icon={faMedal}
                    />
                  </View>
                  <Text style={styles.descriptionRank}>
                    Kế hoạch tập luyện trung bình được thiết kế cho những người
                    tập từ 4-6 lần một tuần.
                  </Text>
                </View>
                <TouchableWithoutFeedback onPress={hideModalInfo}>
                  <Text style={styles.txtUnderstand}>ĐÃ HIỂU</Text>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  containModal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.6)',
    justifyContent: 'center',
  },
  viewInfo: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#323746',
    borderRadius: 5,
    paddingBottom: 20,
  },
  rowRank: {
    flexDirection: 'row',
    height: 70,
    width: '100%',
    alignSelf: 'center',
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 5,
    paddingHorizontal: '5%',
  },
  txtRank: {
    alignSelf: 'center',
    fontSize: 17,
    fontWeight: '600',
    marginLeft: 10,
    color: '#fff',
  },
  boundary: {
    width: '100%',
    height: 15,
  },
  leftRowRank: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  descriptionRank: {
    alignSelf: 'center',
    fontSize: 14,
    marginLeft: 5,
    color: '#fff',
    width: width * 0.9 - 60,
  },
  txtUnderstand: {
    alignSelf: 'center',
    marginTop: 10,
    fontSize: 22,
    fontWeight: '600',
    color: '#77C38A',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },
});

export default Index;
