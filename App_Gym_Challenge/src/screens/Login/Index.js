/* eslint-disable react-native/no-inline-styles */
import {Formik} from 'formik';
import React, {useState, useRef} from 'react';
import {
  Alert,
  Dimensions,
  Image,
  ImageBackground,
  Modal,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {DotIndicator} from 'react-native-indicators';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as yup from 'yup';
import InputPrimary from '../../commons/InputPrimary';
import {login, requireOtpAgain, sendOtp} from '../../services/apiAuth';
import {useSelector, useDispatch} from 'react-redux';
import {changeInfo, changeStatusLogin} from '../../redux/authSlice';
import Toast, {DURATION} from 'react-native-easy-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import jwt_decode from 'jwt-decode';
import {apiAddSync, apiSync, apiUpdateSync} from '../../services/apiSync';
import {getDataExercise, getListKeyExercise, setDataByKey, setKeyExercise} from '../../utils/strorage';
import { requestReload } from '../../redux/dataSlice';

const {width, height} = Dimensions.get('window');

const getRequireMessage = (name, value) => `${name} không được để trống!`;
const getMinLengMessage = (name, value) =>
  `${name} phải chứa ít nhất ${value} kí tự!`;
const getMaxLengMessage = (name, value) =>
  `${name} chứa nhiều nhất ${value} kí tự!`;

const usernameSignIn = yup
  .string()
  .required(getRequireMessage('Tên đăng nhập'));

const passwordSignIn = yup
  .string()
  .min(6, getMinLengMessage('Mật khẩu', 6))
  .max(28, getMaxLengMessage('Mật khẩu', 28))
  .required(getRequireMessage('Mật khẩu'));

const singInValidate = yup.object().shape({
  username: usernameSignIn,
  password: passwordSignIn,
});

export const TextValidate = props => {
  const {errors} = props;
  return (
    <Text
      style={[
        styles.textValidate,
        {
          marginBottom: 15,
        },
      ]}>
      {errors}
    </Text>
  );
};

const Index = () => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [passWord, setPassWord] = useState('');
  const [isSecureTextEntry, setIsSecureTextEntry] = useState(true);
  const [errors, setErrors] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [otp, setOtp] = useState('');
  const [visiableModalOtp, setVisiableModalOtp] = useState(false);
  const [emailSignIn, setEmailSignIn] = useState('');
  const [userId, setUserId] = useState('');

  const refToast = useRef(null);
  const refToast2 = useRef(null);

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const showPassword = () => {
    setIsSecureTextEntry(!isSecureTextEntry);
  };

  const syncData = async (isLogout = false) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await apiSync({token});
      if (res?.status === 1) {
        const dataApi = res.data;
        let data = {};
        if (dataApi.length == 1) {
          if (dataApi[0]?.data) {
            data = JSON.parse(dataApi[0]?.data);
          }
        }

        let dataExercising = data?.dataExercising || [];
        let dataHistory = data?.dataHistory || [];
        let dataDrink = data?.dataDrink || [];

        if (dataExercising) {
          const listCha = [];
          dataExercising.map(item => {
            listCha.push(item.id);
            setDataByKey(item.id, item);
          });
          setKeyExercise(listCha);
        }
        if (dataHistory) {
          await AsyncStorage.setItem('@history', JSON.stringify(dataHistory));
        }
        if (dataDrink) {
          await AsyncStorage.setItem(
            '@historyDrink',
            JSON.stringify(dataDrink),
          );
        }
        dispatch(requestReload());
      } else {
        Alert.alert('', 'Đã có lỗi xảy ra!');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onSignIn = async (values, resetForm) => {
    try {
      setIsLoading(true);
      const body = {
        password: values.password,
        userName: values.username.toUpperCase(),
      };
      const res = await login(body);
      console.log('res', res);
      if (res.status === 1) {
        await AsyncStorage.setItem('@token', res.data.token);
        const dataDecode = jwt_decode(res.data.token);
        dispatch(changeInfo(dataDecode));
        syncData(false);
        navigation.goBack();
      } else {
        if (res.status == 2) {
          await requireOtpAgain({
            email: res?.data?.email,
            idUser: res?.data?.idUser,
          });
          setVisiableModalOtp(true);
          setEmailSignIn(res?.data?.email);
          setUserId(res?.data?.idUser);
          refToast2.current.show(
            'Bạn hãy nhập mã otp gửi về mail đăng kí để kích hoạt',
            2000,
          );
        } else refToast.current.show(res.message, 2000);
      }
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      refToast.current.show('Đã có lỗi xảy ra thử lại sau', 2000);
      console.log(error);
    }
  };

  const onPressRegister = () => {
    navigation.navigate('Register');
  };

  const onPressForgot = () => {
    navigation.navigate('Forgot');
  };

  const reSendOtp = async () => {
    const res = await requireOtpAgain({email: emailSignIn, idUser: userId});
    if (res.status === 1) {
      refToast2.current.show('Gửi lại mã thành công', 2000);
    } else {
      refToast2.current.show('Gửi lại mã không thành công', 2000);
    }
  };

  const submitOtp = async () => {
    const res = await sendOtp({idUser: userId, otp});
    if (res.status === 1) {
      setVisiableModalOtp(false);
      refToast.current.show('Kích hoạt thành công', 2000);
    } else {
      refToast2.current.show('Kích hoạt không thành công', 2000);
    }
  };

  return (
    <ImageBackground
      style={styles.background}
      source={require('../../assets/images/image_background.png')}>
      <SafeAreaView style={styles.container}>
        <StatusBar />
        <KeyboardAwareScrollView
          contentContainerStyle={styles.viewKeyboard}
          showsVerticalScrollIndicator={false}>
          <View style={{alignItems: 'center', height: 200}} />
          <Formik
            initialValues={{
              username: __DEV__ ? phoneNumber : phoneNumber,
              password: __DEV__ ? passWord : passWord,
            }}
            enableReinitialize={true}
            onSubmit={(values, {resetForm}) => onSignIn(values, resetForm)}
            validationSchema={singInValidate}>
            {({
              values,
              handleChange,
              errors,
              setFieldTouched,
              touched,
              isValid,
              handleSubmit,
            }) => (
              <View style={styles.blockInput}>
                <InputPrimary
                  label={'Tên đăng nhập'}
                  placeholder={'Nhập tên đăng nhập'}
                  value={values.username}
                  onChangeText={handleChange('username')}
                  onBlur={() => setFieldTouched('username')}
                  isValid={touched.username && !errors.username}
                  error={touched.username && errors.username && errors.username}
                />
                <InputPrimary
                  label={'Mật khẩu'}
                  placeholder={'*******'}
                  value={values.password}
                  onChangeText={handleChange('password')}
                  onBlur={() => setFieldTouched('password')}
                  isValid={touched.password && !errors.password}
                  error={touched.password && errors.password && errors.password}
                  secureTextEntry={isSecureTextEntry}
                  suffixIconAction={() => showPassword()}
                  suffixIcon={require('../../assets/icons/eye.png')}
                />
                {!isLoading ? (
                  <TouchableWithoutFeedback onPress={handleSubmit}>
                    <View style={styles.buttonSubmit}>
                      <Text style={styles.txtSubmit}>Đăng nhập</Text>
                    </View>
                  </TouchableWithoutFeedback>
                ) : (
                  <View style={styles.viewDotIn}>
                    <DotIndicator color={'#54CEF5'} size={6} count={8} />
                  </View>
                )}

                <TouchableWithoutFeedback onPress={onPressRegister}>
                  <Text style={styles.txtRegister}>
                    Bạn chưa có tài khoản? Hãy{' '}
                    <Text style={styles.txtRegisterColor}>đăng kí</Text>
                  </Text>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={onPressForgot}>
                  <Text style={styles.txtForgotColor}>Quên mật khẩu</Text>
                </TouchableWithoutFeedback>
              </View>
            )}
          </Formik>
          <Toast position="top" ref={refToast} />
        </KeyboardAwareScrollView>
      </SafeAreaView>

      <Modal transparent visible={visiableModalOtp}>
        <TouchableWithoutFeedback onPress={() => setVisiableModalOtp(false)}>
          <View style={styles.containerModal}>
            <TouchableWithoutFeedback>
              <View style={styles.wallet}>
                <Text style={styles.titleOtp}>Nhập mã OTP</Text>
                <TextInput style={styles.inputOtp} onChangeText={setOtp} />
                <TouchableWithoutFeedback onPress={reSendOtp}>
                  <Text style={styles.txtResend}>Gửi lại mã otp</Text>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={submitOtp}>
                  <View style={styles.btSubmitOtp}>
                    <Text style={styles.txtBtSubmitOtp}>Gửi mã OTP</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
            <Toast position="top" ref={refToast2} />
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 1,
  },
  background: {
    flex: 1,
  },
  title: {
    fontFamily: 'Nunito-Regular',
    fontSize: 14,
    color: '#000',
    fontWeight: 'bold',
    alignItems: 'center',
  },
  btnLogin: {
    backgroundColor: '#54CEF5',
    height: 40,
    zIndex: 10,
    borderRadius: 5,
    marginTop: 10,
    borderWidth: 0,
  },
  viewKeyboard: {
    flexGrow: 1,
    flexDirection: 'column',
  },
  styleTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: 14,
    color: '#FFF',
  },
  viewDotIn: {
    height: 20,
    marginTop: 8,
  },
  titleTea: {
    alignSelf: 'center',
    fontFamily: 'Nunito-Bold',
    fontSize: 28,
    lineHeight: 38,
    color: '#000',
  },
  teaDesc: {
    fontFamily: 'Nunito',
    fontSize: 16,
    lineHeight: 22,
    textAlign: 'center',
    color: '#757575',
    alignSelf: 'center',
    width: 280,
  },
  btn_login_social: {
    backgroundColor: '#2E3192',
    flexDirection: 'row',
    height: 30,
    alignItems: 'center',
    width: '45%',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: '3%',
    borderRadius: 4,
    marginTop: height * 0.02,
  },
  icon_social: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  text_social: {
    fontSize: 10,
    fontFamily: 'Nunito-Regular',
    color: 'white',
    marginLeft: 5,
  },
  buttonSubmit: {
    height: 50,
    backgroundColor: '#5AC685',
    justifyContent: 'center',
    width: '80%',
    alignSelf: 'center',
    borderRadius: 30,
  },
  txtSubmit: {
    alignSelf: 'center',
    fontSize: 18,
    color: 'white',
    fontWeight: '600',
  },
  blockInput: {
    width: '90%',
    height: 360,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: '3%',
    borderRadius: 20,
  },
  txtRegister: {
    alignSelf: 'center',
    fontSize: 14,
    color: 'white',
    fontWeight: '600',
    marginTop: 10,
  },
  txtRegisterColor: {
    color: '#5AC685',
  },
  txtForgotColor: {
    alignSelf: 'center',
    fontSize: 14,
    color: '#5AC685',
    fontWeight: '600',
    marginTop: 10,
  },
  containerModal: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  wallet: {
    width: '90%',
    alignSelf: 'center',
    height: 200,
    backgroundColor: '#181c29',
    borderRadius: 10,
  },
  titleOtp: {
    fontSize: 18,
    fontWeight: '500',
    marginTop: 10,
    marginLeft: '5%',
    color: '#5AC685',
  },
  inputOtp: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 16,
    borderWidth: 1,
    height: 40,
    borderColor: '#5AC685',
    borderRadius: 5,
    paddingHorizontal: '5%',
    color: '#fff',
    fontWeight: '500',
  },
  txtResend: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#5AC685',
    fontWeight: '600',
    marginTop: 10,
  },
  btSubmitOtp: {
    width: '60%',
    height: 40,
    borderRadius: 30,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 30,
    backgroundColor: '#5AC685',
  },
  txtBtSubmitOtp: {
    alignSelf: 'center',
    fontSize: 18,
    color: '#fff',
    fontWeight: '600',
  },
});

export default Index;
