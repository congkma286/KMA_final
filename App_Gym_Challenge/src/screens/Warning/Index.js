import moment from 'moment';
import React, {useEffect, useState, useRef} from 'react';
import {
  Dimensions,
  Keyboard,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
  Switch,
  ImageBackground,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import PushNotification from 'react-native-push-notification';
import Toast from 'react-native-toast-message';
import Header from '../../commons/Header';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const {height} = Dimensions.get('window');

const Index = () => {
  const [isVisibleDatePick, setIsVisibleDatePick] = useState(false);

  const [timeStart, setTimeStart] = useState(new Date());
  const [timeEnd, setTimeEnd] = useState(new Date());
  const [step, setStep] = useState('30');

  const refInputStep = useRef(null);

  const [typeDatePick, setTypeDatePick] = useState('start');

  const [isEnabled, setIsEnabled] = useState(false);

  const navigation = useNavigation();

  const isValidation = () => {
    if (Math.round((timeEnd.getTime() - timeStart.getTime()) / 60000) <= 0) {
      Toast.show({
        type: 'error',
        text1: 'Thất bại',
        text2: 'Thời gian khởi đầu và kết thúc không hợp lệ',
      });
      return false;
    }
    if (!step) {
      Toast.show({
        type: 'error',
        text1: 'Thất bại',
        text2: 'Chu kì nhắc nhở không hợp lệ',
      });
      return false;
    }
    return true;
  };

  const submit = async () => {
    try {
      if (isValidation()) {
        await AsyncStorage.setItem('@isNotiWater', 'true');
        PushNotification.cancelAllLocalNotifications();
        const minuter = Math.round(
          (timeEnd.getTime() - timeStart.getTime()) / 60000,
        );
        const numStep = Number(step);
        for (let index = 0; index < minuter / numStep + 1; index++) {
          PushNotification.localNotificationSchedule({
            title: 'Hãy uống nước 🥤',
            message:
              'Uống một ly nước ngay bây giờ để giúp bạn giữ dáng và giảm bớt mệt mỏi', // (required)
            date: new Date(timeStart.getTime() + index * 60000 * numStep), // in 60 secs
            allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
            repeatType: 'day',
            repeatTime: 1, // (optional) Increment of configured repeatType. Check 'Repeating Notifications' section for more info.
          });
        }
        Toast.show({
          type: 'success',
          text1: 'Thành công',
          text2: 'Đã tạo lịch nhắc uống nước 🥤',
        });
        setTimeout(() => {
          navigation.goBack();
        }, 1000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleConfirm = value => {
    setIsVisibleDatePick(false);
    switch (typeDatePick) {
      case 'start':
        setTimeStart(value);
        break;
      case 'end':
        setTimeEnd(value);
        break;
      default:
        break;
    }
  };

  const onCancel = () => {
    setIsVisibleDatePick(false);
  };

  const onPressTimeStart = () => {
    setTypeDatePick('start');
    setIsVisibleDatePick(true);
  };

  const onPressTimeEnd = () => {
    setTypeDatePick('end');
    setIsVisibleDatePick(true);
  };

  const onPressTimeStep = () => {
    refInputStep.current.focus();
  };

  const hideKeyboard = () => {
    Keyboard.dismiss();
  };

  const toggleSwitch = () => {
    if (isEnabled) {
      clearNoti();
    }
    setIsEnabled(!isEnabled);
  };

  const clearNoti = async () => {
    await AsyncStorage.setItem('@isNotiWater', 'false');
    PushNotification.cancelAllLocalNotifications();
  };

  const getValues = async () => {
    const is = await AsyncStorage.getItem('@isNotiWater');
    if (is === 'true') {
      setIsEnabled(true);
    } else {
      setIsEnabled(false);
    }
  };

  useEffect(() => {
    getValues();
  }, []);

  return (
    <TouchableWithoutFeedback onPress={hideKeyboard}>
      <ImageBackground
        style={styles.container}
        source={require('../../assets/images/hinhnen2.png')}>
        <SafeAreaView />
        <Header
          title={'Cài đặt uống nước'}
          type={'iconLeft'}
          textColor={'#fff'}
          color={'#fff'}
        />

        <TouchableWithoutFeedback onPress={onPressTimeStart}>
          <View style={[styles.row, styles.top]}>
            <Text style={styles.title}>Bật cảnh báo</Text>
            <Switch
              trackColor={{false: '#767577', true: '#5AC685'}}
              thumbColor={isEnabled ? '#181c29' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </TouchableWithoutFeedback>

        {isEnabled && (
          <>
            <View style={styles.line} />

            <TouchableWithoutFeedback onPress={onPressTimeStart}>
              <View style={[styles.row]}>
                <Text style={styles.title}>Khởi đầu</Text>
                <Text style={styles.txtValue}>
                  {moment(timeStart).format('HH:mm')}
                </Text>
              </View>
            </TouchableWithoutFeedback>

            <View style={styles.line} />

            <TouchableWithoutFeedback onPress={onPressTimeEnd}>
              <View style={[styles.row]}>
                <Text style={styles.title}>Kết thúc</Text>
                <Text style={styles.txtValue}>
                  {moment(timeEnd).format('HH:mm')}
                </Text>
              </View>
            </TouchableWithoutFeedback>

            <View style={styles.line} />

            <TouchableWithoutFeedback onPress={onPressTimeStep}>
              <View style={[styles.row]}>
                <Text style={styles.title}>Chu kì nhắc nhở</Text>
                <View style={styles.rowInput}>
                  <TextInput
                    ref={refInputStep}
                    style={styles.txtValue}
                    value={step}
                    onChangeText={setStep}
                    keyboardType="numeric"
                  />
                  <Text style={styles.txtValue}> phút</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback onPress={submit}>
              <View style={styles.btSubmit}>
                <Text style={styles.txtBtSubmit}>Nhắc tôi</Text>
              </View>
            </TouchableWithoutFeedback>
          </>
        )}

        <DateTimePickerModal
          isVisible={isVisibleDatePick}
          mode="time"
          date={typeDatePick === 'start' ? timeStart : timeEnd}
          onConfirm={handleConfirm}
          onCancel={onCancel}
          locale="vi_VN"
          headerTextIOS={'Chọn ngày'}
          confirmTextIOS={'Xác nhận'}
          cancelTextIOS={'Huỷ'}
        />
      </ImageBackground>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  box: {
    width: '90%',
    height: 100,
    backgroundColor: 'red',
    alignSelf: 'center',
    top: 200,
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'center',
    width: '90%',
    justifyContent: 'space-between',
  },
  rowInput: {
    flexDirection: 'row',
  },
  top: {
    marginTop: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    color: '#fff',
    alignSelf: 'center',
  },
  txtValue: {
    fontSize: 16,
    fontWeight: '600',
    color: '#5AC685',
    alignSelf: 'center',
  },
  line: {
    width: '90%',
    alignSelf: 'center',
    height: 2,
    backgroundColor: '#2E3137',
    marginVertical: 14,
  },
  btSubmit: {
    width: '70%',
    height: 40,
    alignSelf: 'center',
    backgroundColor: '#5AC685',
    borderRadius: 10,
    justifyContent: 'center',
    position: 'absolute',
    top: height - 100,
  },
  txtBtSubmit: {
    alignSelf: 'center',
    fontSize: 16,
    color: '#fff',
    fontWeight: '700',
  },
});

export default Index;
