import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ImageBackground,
  FlatList,
  Dimensions,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Header from '../../commons/Header';
import {getDataExercise, getLikeEx} from '../../utils/strorage';
import {getImgById} from '../../constants/exerciseFormat';
import Progress from '../../components/Progress';
import {useNavigation} from '@react-navigation/native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

const {width} = Dimensions.get('window');

const Index = () => {
  const [data, setData] = useState([]);
  const navigation = useNavigation();

  const getData = async () => {
    const dataLike = await getLikeEx();
    const dataStrorage = await getDataExercise(dataLike);
    setData(dataStrorage);
  };

  useEffect(() => {
    getData();
  }, []);

  const onPressItem = item => {
    if (item.level) {
      navigation.navigate('Daily', {data: item, level: item.level});
    } else {
      navigation.navigate('Level', {item});
    }
  };

  const renderItem = ({item}) => {
    return (
      <TouchableWithoutFeedback onPress={() => onPressItem(item)}>
        <ImageBackground
          style={styles.bannerRender}
          source={getImgById(item?.id)}>
          <View style={styles.itemRender}>
            <Text style={styles.txtName}>{item?.title}</Text>
            {item?.progress ? (
              <View style={styles.tabProgress}>
                <Progress width={width * 0.8} progress={item?.progress} />
                <Text style={styles.txtProgress}>{item?.progress}%</Text>
              </View>
            ) : (
              <></>
            )}
          </View>
        </ImageBackground>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <ImageBackground
      source={require('../../assets/images/hinhnen2.png')}
      style={styles.contain}>
      <SafeAreaView />
      <Header
        title={'Bài tập ưa thích'}
        type={'iconLeft'}
        textColor={'#fff'}
        color={'#fff'}
      />
      <FlatList data={data} renderItem={renderItem} />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: '#181c29',
  },
  bannerRender: {
    width: width * 0.9,
    alignSelf: 'center',
    height: 100,
    marginTop: 20,
    borderRadius: 10,
    // backgroundColor: '#fff',
  },
  itemRender: {
    width: '100%',
    alignSelf: 'center',
    height: 100,
    borderRadius: 3,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  txtName: {
    fontSize: 16,
    fontWeight: '800',
    color: '#fff',
    top: '50%',
    left: '5%',
    fontFamily: 'Nunito',
    transform: [{translateY: -7}],
  },
  tabProgress: {
    top: '50%',
  },
  txtProgress: {
    marginTop: 3,
    fontSize: 14,
    fontWeight: '700',
    color: '#fff',
    left: '5%',
  },
});

export default Index;
