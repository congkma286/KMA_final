import React, {useRef, useState} from 'react';
import {KeyboardAvoidingView, Platform, StyleSheet, View} from 'react-native';
import WebView from 'react-native-webview';

var checkRatio = false;

const GptScreen = () => {
  const ref_flatlist = useRef();
  const [ratio, setRatio] = useState(1);

  const hideAds = () => {
    return `
      let marquee =  document.getElementsByClassName('marquee').length;
      for (let index = 0; index < marquee; index++) {
        document.getElementsByClassName('marquee')[index].style.display = 'none';
      }


      window.addEventListener('scroll',(event) => {
        let adsbygoogle =  document.getElementsByClassName('adsbygoogle').length;
        for (let index = 0; index < adsbygoogle; index++) {
          document.getElementsByClassName('adsbygoogle')[index].style.display = 'none';
        }
      });

      document.getElementsByClassName('header')[0].style.display = 'none';
      document.getElementsByClassName('slogan')[0].style.display = 'none';
      
      `;
  };

  const onWebViewMessage = event => {
    if (checkRatio) {
      return;
    }
    // setRatio(AppStyle.Screen.FullHeight / Number(event.nativeEvent.data));
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{
        flex: 1,
        backgroundColor: '#F6FAFF',
      }}>
      <View style={styles.container}>
        <WebView
          source={{uri: 'https://chatgptweb.org/'}}
          bounces={false}
          showsVerticalScrollIndicator={false}
          style={{
            width: '100%',
            height: '100%',
          }}
          onMessage={onWebViewMessage}
          javaScriptEnabled={true}
          injectedJavaScript={hideAds()}
        />
      </View>
    </KeyboardAvoidingView>
  );
};

export default GptScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
