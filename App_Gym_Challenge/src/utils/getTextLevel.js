const getTextLevel = level => {
  switch (level) {
    case 1:
      return 'Dễ';
    case 2:
      return 'Trung bình';
    case 3:
      return 'Khó';
    // case 4:
    //   return 'Trung bình 2';
    // case 5:
    //   return 'Khó 1';
    // case 6:
    //   return 'Khó 2';
    default:
      return '';
  }
};

export default getTextLevel;
