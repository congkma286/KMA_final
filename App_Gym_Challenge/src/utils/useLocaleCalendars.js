import {LocaleConfig} from 'react-native-calendars';
// import {useTranslation} from 'react-i18next';

const useLocaleCalendars = () => {
  //   const {t} = useTranslation();
  LocaleConfig.locales['fr'] = {
    monthNames: [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre',
    ],
    monthNamesShort: [
      'Janv.',
      'Févr.',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juil.',
      'Août',
      'Sept.',
      'Oct.',
      'Nov.',
      'Déc.',
    ],
    dayNames: [
      'Dimanche',
      'Lundi',
      'Mardi',
      'Mercredi',
      'Jeudi',
      'Vendredi',
      'Samedi',
    ],
    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    today: "Aujourd'hui",
  };

  LocaleConfig.locales['vi'] = {
    monthNames: [
      'Tháng một',
      'Tháng hai',
      'Tháng ba',
      'Tháng bốn',
      'Tháng năm',
      'Tháng sáu',
      'Tháng bảy',
      'Tháng tám',
      'Tháng chín',
      'Tháng mười',
      'Tháng mời một',
      'Tháng mười hai',
    ],
    monthNamesShort: [
      'T.Một.',
      'T.Hai.',
      'T.Ba',
      'T.Tư',
      'T.Năm',
      'T.Sáu',
      'T.Bảy.',
      'T.Tám',
      'T.Chín.',
      'T.Mười.',
      'T.MMột.',
      'T.MHai.',
    ],
    dayNames: [
      'Chủ nhật',
      'Thứ 2',
      'Thứ 3',
      'Thứ 4',
      'Thứ 5',
      'Thứ 6',
      'Thứ 7',
    ],
    dayNamesShort: ['CN', ' T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    today: 'Hôm nay',
  };

  const setUp = () => {
    // LocaleConfig.defaultLocale = t('location');
    LocaleConfig.defaultLocale = 'vi';
  };

  return {
    setup: setUp,
  };
};

export default useLocaleCalendars;
