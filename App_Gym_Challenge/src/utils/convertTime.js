const convertTime = d => {
  d = Number(d);
  var m = Math.floor((d % 3600) / 60);
  var s = Math.floor((d % 3600) % 60);

  var mDisplay = m > 0 ? `0${m}`.slice(-2) : '00';
  var sDisplay = s > 0 ? `0${s}`.slice(-2) : '00';
  return mDisplay + ':' + sDisplay;
};

export default convertTime;
