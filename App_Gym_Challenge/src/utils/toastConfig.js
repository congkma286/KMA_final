/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {SafeAreaView, Text, View} from 'react-native';

import {BaseToast, ErrorToast} from 'react-native-toast-message';

export const toastConfig = {
  success: props => (
    <SafeAreaView>
      <BaseToast
        {...props}
        style={{borderLeftColor: '#5AC685'}}
        contentContainerStyle={{paddingHorizontal: 15}}
        text1Style={{
          fontSize: 15,
          fontWeight: '400',
        }}
      />
    </SafeAreaView>
  ),

  error: props => (
    <SafeAreaView>
      <ErrorToast
        {...props}
        text2NumberOfLines={2}
        text1Style={{
          fontSize: 17,
        }}
        text2Style={{
          fontSize: 15,
        }}
      />
    </SafeAreaView>
  ),

  tomatoToast: ({text1, props}) => (
    <SafeAreaView>
      <View style={{height: 60, width: '100%', backgroundColor: 'tomato'}}>
        <Text>{text1}</Text>
        <Text>{props.uuid}</Text>
      </View>
    </SafeAreaView>
  ),
};
