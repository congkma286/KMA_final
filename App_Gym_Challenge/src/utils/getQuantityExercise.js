export const getTime = time => {
  if (getTime < 60) {
    return '00:' + time;
  } else {
    const minuter = Math.floor(time / 60);
    const second = time - minuter * 60;
    const stringMinuter = ('00' + minuter).slice(-2);
    const stringSecond = ('00' + second).slice(-2);
    return stringMinuter + ':' + stringSecond;
  }
};

export const getRep = rep => {
  return 'x ' + rep;
};
