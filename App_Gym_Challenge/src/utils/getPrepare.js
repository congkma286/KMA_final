import {dataTapBung} from '../constants/TAP_BUNG';
import {dataTapTay} from '../constants/TAP_TAY';
import {dataTapChan} from '../constants/TAP_CHAN';
import {dataTapMong} from '../constants/TAP_MONG';
import {dataTapToanThan} from '../constants/TAP_TOAN_THAN';

function layNgauNhien(arr, n) {
  if (n >= arr.length) {
    return arr;
  } else {
    var shuffled = arr.slice();
    for (var i = shuffled.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = shuffled[i];
      shuffled[i] = shuffled[j];
      shuffled[j] = temp;
    }
    return shuffled.slice(0, n);
  }
}

const getPrepare = props => {
  let data = [];
  const {id, level, day} = props;

  const timeIncre = Math.ceil(day / 10) - 1;

  const getTimeByLevel = lv => {
    switch (lv) {
      case 1:
        return 30 + timeIncre;
      case 2:
        return 45 + timeIncre;
      case 3:
        return 60 + timeIncre;
      default:
        break;
    }
  };

  const getCalo = (lv, numCalo) => {
    const calo = Math.floor((((2 + lv) * 15) / 5) * numCalo);
    return calo;
  };

  switch (id) {
    case 'TAP_BUNG':
      if (level == 1) {
        dataTapBung.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 12,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 2) {
        dataTapBung.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 17,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 3) {
        dataTapBung.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 25,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      }
      return layNgauNhien(data, 4);
    case 'TAP_CHAN':
      if (level == 1) {
        dataTapChan.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 12,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 2) {
        dataTapChan.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 17,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 3) {
        dataTapChan.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 25,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      }
      return layNgauNhien(data, 4);
    case 'TAP_MONG':
      if (level == 1) {
        dataTapMong.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 12,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 2) {
        dataTapMong.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 17,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 3) {
        dataTapMong.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 25,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      }
      return layNgauNhien(data, 4);
    case 'TAP_TAY':
      if (level == 1) {
        dataTapTay.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 12,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 2) {
        dataTapTay.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 17,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 3) {
        dataTapTay.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 25,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      }
      return layNgauNhien(data, 4);
    case 'TAP_TOAN_THAN':
      if (level == 1) {
        dataTapToanThan.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 12,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 2) {
        dataTapToanThan.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 17,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      } else if (level == 3) {
        dataTapToanThan.map(d => {
          if (data.type === 1) {
            data.push({
              ...d,
              time: 0,
              rep: 25,
              calo: getCalo(level, d.numCalo),
            });
          } else {
            data.push({
              ...d,
              time: getTimeByLevel(level),
              rep: 0,
              calo: getCalo(level, d.numCalo),
            });
          }
        });
      }
      return layNgauNhien(data, 4);
    default:
      return [];
  }
};

export default getPrepare;
