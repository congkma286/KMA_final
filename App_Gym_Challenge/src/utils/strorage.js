import AsyncStorage from '@react-native-async-storage/async-storage';

export const getWeigh = async () => {
  try {
    const data = await AsyncStorage.getItem('@parameterUser');
    const dataJson = JSON.parse(data);
    return dataJson;
  } catch (error) {
    return {};
  }
};

export const setWeigh = async data => {
  try {
    await AsyncStorage.setItem('@parameterUser', JSON.stringify(data));
  } catch (error) {}
};

export const getLikeEx = async () => {
  try {
    const data = await AsyncStorage.getItem('@likeExercising');
    const dataJson = JSON.parse(data);
    return dataJson;
  } catch (error) {
    return {};
  }
};

export const setLikeEx = async data => {
  try {
    await AsyncStorage.setItem('@likeExercising', JSON.stringify(data));
  } catch (error) {}
};

export const setDataByKey = async (key, data) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(data));
    console.log('lưu', key);
  } catch (error) {}
};

export const setKeyExercise = data => {
  try {
    //['TAP_BUNG','TAP_TOAN_THAN'] data
    AsyncStorage.setItem('@key_exercise', JSON.stringify(data));
  } catch (error) {}
};

export const getListKeyExercise = async () => {
  try {
    const dataString = await AsyncStorage.getItem('@key_exercise');
    if (dataString) {
      const data = JSON.parse(dataString);
      return data;
    }
    return [];
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const getDataExercise = async listKey => {
  try {
    if (listKey.length > 0) {
      const data = await AsyncStorage.multiGet(listKey);
      const dataNeed = [];
      data.map(i => dataNeed.push(JSON.parse(i[1])));
      return dataNeed;
    }
    return [];
  } catch (error) {
    console.log(error);
    return {};
  }
};
