const getIconExercise = type => {
  switch (type) {
    case 'RUNrHkbP4Pc':
      return require('../assets/images/icon-exercise-1.png');
    case 'swOyWKk7Oko':
      return require('../assets/images/icon-exercise-2.png');
    case 'dGKbTKLnym4':
      return require('../assets/images/icon-exercise-3.png');
    default:
      return require('../assets/images/icon-exercise-3.png');
      break;
  }
};

export default getIconExercise;
