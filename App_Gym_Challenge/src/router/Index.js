import {createStackNavigator} from '@react-navigation/stack';
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Home from '../screens/Home/Index';
import Login from '../screens/Login/Index';
import BottomTab from './BottomTab';
import Level from '../screens/Level/Index';
import Daily from '../screens/Daily/Index';
import Prepare from '../screens/Prepare/Index';
import Exercising from '../screens/Exercising/Index';
import Introduce from '../screens/Introduce/Index';
import Register from '../screens/Register/Index';
import Warning from '../screens/Warning/Index';
import Contact from '../screens/Contact/Index';
import ContactChatbot from '../screens/ContactBot/Index';
import ContactAdmin from '../screens/ContactAdmin/Index';
import ListContact from '../screens/ListContact/Index';
import Product from '../screens/Product/Index';
import LikeExercising from '../screens/LikeExercising/Index';
import Forgot from '../screens/Forgot/Index';
import {checkToken} from '../services/apiAuth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {changeInfo} from '../redux/authSlice';
import ChatGPT from '../screens/ChatGPT/Index';
import ChangePassword from '../screens/ChangePassword/Index';
import DrinkWater from '../screens/DrinkWater/Index';

const Stack = createStackNavigator();

const Index = () => {
  const isLogin = useSelector(state => state.auth.isLogin);
  const dispatch = useDispatch();

  const loginWithToken = async () => {
    const token = await AsyncStorage.getItem('@token');
    const res = await checkToken({token: token});
    if (res?._id) {
      dispatch(changeInfo(res));
    }
  };

  useEffect(() => {
    loginWithToken();
  }, []);

  return true ? (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="BottomTab" component={BottomTab} />
      <Stack.Screen name="Level" component={Level} />
      <Stack.Screen name="Daily" component={Daily} />
      <Stack.Screen name="Prepare" component={Prepare} />
      <Stack.Screen name="Exercising" component={Exercising} />
      <Stack.Screen name="Introduce" component={Introduce} />
      <Stack.Screen name="Warning" component={Warning} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Forgot" component={Forgot} />
      <Stack.Screen name="Contact" component={Contact} />
      <Stack.Screen name="ListContact" component={ListContact} />
      <Stack.Screen name="ContactAdmin" component={ContactAdmin} />
      <Stack.Screen name="ContactChatBot" component={ContactChatbot} />
      <Stack.Screen name="Product" component={Product} />
      <Stack.Screen name="LikeExercising" component={LikeExercising} />
      <Stack.Screen name="ChatGPT" component={ChatGPT} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="DrinkWater" component={DrinkWater} />
      
    </Stack.Navigator>
  ) : (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
};

export default Index;
