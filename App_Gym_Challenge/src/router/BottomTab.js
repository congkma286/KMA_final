import {faTrophy, faUser, faCalendar} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React from 'react';
import Home from '../screens/Home/Index';
import User from '../screens/User/Index';
import CalendarScreen from '../screens/Calendar/Index';
import Daily from '../screens/Daily/Index';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#5cc87f',
        tabBarInactiveTintColor: '#b3b3b3',
        tabBarStyle: {backgroundColor: '#262a39'},
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Thử thách',
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} size={22} icon={faTrophy} />
          ),
        }}
      />
      <Tab.Screen
        name="CalendarScreen"
        component={CalendarScreen}
        options={{
          tabBarLabel: 'Hàng ngày',
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} size={22} icon={faCalendar} />
          ),
        }}
      />
      <Tab.Screen
        name="User"
        component={User}
        options={{
          tabBarLabel: 'Cá nhân',
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} size={22} icon={faUser} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTab;
