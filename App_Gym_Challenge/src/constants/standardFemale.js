export const standardFemale = [
  {height: '150', weight: '41.85 -55.80kg'},
  {height: '151', weight: '42.41 – 56.55kg'},
  {height: '152', weight: '42.97 – 57.30kg'},
  {height: '153', weight: '43.54 – 58.05kg'},
  {height: '154', weight: '44.11 – 58.82kg'},
  {height: '155', weight: '44.69 – 59.58kg'},
  {height: '156', weight: '45.26 – 60.35kg'},
  {height: '157', weight: '45.85 – 61.13kg'},
  {height: '158', weight: '46.43 – 61.91kg'},
  {height: '159', weight: '47.02 – 62.70kg'},
  {height: '160', weight: '47.62 – 63.49kg'},
  {height: '161', weight: '48.21 – 64.28kg'},
  {height: '162', weight: '48.81 – 65.09kg'},
  {height: '163', weight: '49.42 – 65.89kg'},
  {height: '164', weight: '50.03 – 66.70kg'},
  {height: '165', weight: '50.64 – 67.52kg'},
  {height: '166', weight: '51.25 – 68.34kg'},
  {height: '167', weight: '51.87 – 69.16kg'},
  {height: '168', weight: '52.50 – 70.0kg'},
  {height: '169', weight: '53.12 – 70.83kg'},
  {height: '170', weight: '53.75 – 71.67kg'},
  {height: '171', weight: '54.39 – 72.52kg'},
  {height: '172', weight: '55.03 – 73.37kg'},
  {height: '173', weight: '55.67 – 74.22kg'},
  {height: '174', weight: '56.31 – 75.08kg'},
  {height: '175', weight: '56.96 – 75.95kg'},
  {height: '176', weight: '57.62 – 76.82kg'},
  {height: '177', weight: '58.27 – 77.7kg'},
  {height: '178', weight: '58.93 – 78.58kg'},
  {height: '179', weight: '59.60 – 79.46kg'},
  {height: '180', weight: '60.26 – 80.35kg'},
];

// BMI = Cân nặng / (Chiều cao)2
// BMI <16: Bạn thuộc nhóm Gầy độ 3;
// 16=<BMI=<16.99: Bạn thuộc nhóm Gầy độ 2;
// 17=<BMI=<18.49: Bạn thuộc nhóm Gầy độ 1;
// 25=<BMI=<29.99: Bạn thuộc nhóm Tiền béo phì;
// 30=<BMI=<34.99: Bạn thuộc nhóm Béo phì độ 1;
// 35=<BMI=<39.99: Bạn thuộc nhóm Béo phì độ 2;
// 40=<BMI: Bạn thuộc nhóm Béo phì độ 3.
