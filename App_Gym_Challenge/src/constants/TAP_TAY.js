export const dataTapTay = [
  {
    title: 'Chống đẩy',
    id: 'R08gYyypGto',
    dep: `Nằm áp xuống sàn lấy tay đỡ thân. Giữ thẳng thân trong khi nâng và hạ thân bằng tay.
Bài tập này giúp tập ngực, vai, cơ tay sau, lưng và chân.`,
    type:1,
    numCalo: 1.4,
  },
  {
    title: 'Tập tay sau trên ghế',
    id: 'JhX1nBnirNw',
    dep: `Từ vị trí bắt đầu, ngồi trên ghế. Rồi di chuyển mông khỏi ghế, tay giữ mép ghế.
Từ từ cong và duỗi thẳng tay để nâng và hạ cơ thể.
Bài tập này có tác dụng tốt cho cơ tay sau.`,
    type:1,
    numCalo: 1.2,
  },
  {
    title: 'Vòng tay',
    id: 'h6GkzSA5tTc',
    dep: `Đứng thẳng duỗi 2 tay thẳng ra 2 bên. Hay tay song song với sàn, vuông góc với thân người
Từ từ xoay 2 tay thành những vòng nhỏ, Hít thở bình thường
Tiếp tục xoay đều sau 15s thì xoay ngược lại
    `,
    type:1,
    numCalo: 1.1,
  },
  {
    title: 'Chống đẩy và chạm chân',
    id: 'YrlXZPNDo3A',
    dep: `Hạ người giống tư thế chống đẩy
Hạ cơ thể cho đến khi khuỷu tay ở 90 độ
Dùng tay phải chạm vào chân trái và đổi bên`,
    type:1,
    numCalo: 1.4,
  },
  {
    title: 'Dãn cơ tay',
    id: 'L9IGOcrdcFk',
    dep: `Đặt tay trái lên lưng
Tay phải nắm lấy khuỷu tay trái và nhẹ nhàng kéo lại
Uốn cong khuỷu tay của bạn giữ phần thân trên của bạn thẳng khi kéo khuỷu tay`,
    type:1,
    numCalo: 1.0,
  },
  {
    title: 'Dãn cơ vai',
    id: '9k0EN2RCGgU',
    dep: `Đặt 1 cánh tay lên trên cơ thể và song song với mặt đất
Sử dụng cánh tay còn lại để kéo nó về phía ngực
Giữ trong vài giây, đổi tay
Hít thở tự nhiên 
    `,
    type:1,
    numCalo: 1.2,
  },
];
