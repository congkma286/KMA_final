export const dataTapMong = [
  {
    title: 'Đứng đá chân sau',
    id: 'pn2EZjEE_ZU',
    dep: `Đứng thẳng tay chống nạnh, sau đó đá chân phải ra sau và duỗi càng xa càng tốt, đồng thời giữ thẳng chân, lưng, đầu thẳng.
Trở lại tư thế ban đầu và chuyển bên`,
    type: 1,
    numCalo: 1.3,
  },
  {
    title: 'Gánh đùi',
    id: '42bFodPahBU',
    dep: `Chân đứng rộng bằng vai, tay để thẳng về trước, hạ thấp cơ thể cho tới khi đùi song song với sàn.
Đầu gối cùng hướng với ngón chân. Quay lại tư thế ban đầu và lặp lại.
Bài tập này giúp tập đùi, hông, cơ đùi trước, gân kheo và thân dưới.`,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Lừa đá chân phải',
    id: '4ranVQDqlaU',
    dep: `Bắt đầu với tư thế quỳ gối chống hai tay. Sau đó nhấc chân phải và ép mông đến hết mức có thể.
Trở lại tư thế bắt đầu và lặp lại từ 10-15 lần cho bài tập này.`,
    type: 1,
    numCalo: 1.3,
  },
  {
    title: 'Lừa đá chân trái',
    id: '4ranVQDqlaU',
    dep: `Bắt đầu với tư thế quỳ gối chống hai tay. Sau đó nhấc chân trái và ép mông đến hết mức có thể.
Trở lại tư thế bắt đầu và lặp lại từ 10-15 lần cho bài này.`,
    type: 1,
    numCalo: 1.3,
  },
  {
    title: 'Cây cầu mông',
    id: '9qo48CYN06w',
    dep: `Nằm ngửa với đầu gối cong và chân để sát mặt sàn. Đặt cánh tay với hai bên thân. Sau đó nhấc mông lên rồi hạ xuống.
Lặp lại 10-15 lần cho bài tập này.`,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Ngồi dựa tường',
    id: 'Yp3ZwACK9v4',
    dep: `Bắt đầu bằng việc dựa lưng vào tường, sau đó trượt xuống cho tới khi đầu gối vuông góc 90 độ.
Dựa lưng vào tường, để tay và cánh tay phía trên chân. Giữ tư thế.
Bài tập này nhằm làm tăng cơ đầu đùi.`,
    type: 2,
    numCalo: 1.5,
  },
];
