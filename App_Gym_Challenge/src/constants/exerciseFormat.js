export const exerciseFormat = [
  {
    title: 'TẬP CƠ BỤNG',
    id: 'TAP_BUNG',
    img: require('../assets/images/banner-5.png'),
    progress: 0,
    day: 0,
    level: 0,
  },
  {
    title: 'TẬP MÔNG',
    id: 'TAP_MONG',
    img: require('../assets/images/banner-3.png'),
    progress: 0,
    day: 0,
    level: 0,
  },
  {
    title: 'TẬP TAY',
    id: 'TAP_TAY',
    img: require('../assets/images/banner-2.png'),
    progress: 0,
    day: 0,
    level: 0,
  },
  {
    title: 'TẬP CHÂN',
    id: 'TAP_CHAN',
    img: require('../assets/images/banner-3.png'),
    progress: 0,
    day: 0,
    level: 0,
  },
  {
    title: 'TẬP TOÀN THÂN',
    id: 'TAP_TOAN_THAN',
    img: require('../assets/images/banner-1.png'),
    progress: 0,
    day: 0,
    level: 0,
  },
];

export const getImgById = id => {
  switch (id) {
    case 'TAP_BUNG':
      return require('../assets/images/banner-5.png');
    case 'TAP_MONG':
      return require('../assets/images/banner-3.png');
    case 'TAP_TAY':
      return require('../assets/images/banner-2.png');
    case 'TAP_CHAN':
      return require('../assets/images/banner-6.png');
    case 'TAP_TOAN_THAN':
      return require('../assets/images/banner-1.png');
    default:
      break;
  }
};
