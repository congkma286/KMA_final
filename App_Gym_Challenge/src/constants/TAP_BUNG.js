//1 là số lượng, 2 là thời gian

export const dataTapBung = [
  {
    title: 'Tập cơ bụng',
    id: 'RUNrHkbP4Pc',
    dep: `Nằm ngửa cong gối và tay duỗi thẳng. Sau đó nhấc thân trên khỏi sàn. Giữ một vài giây rồi từ từ trở lại tư thế ban đầu.
Bài tập này chủ yếu cho các cơ bụng thẳng và cơ liên sườn
        `,
    type: 1,
    numCalo: 1.5,
  },
  {
    title: 'Gập bụng',
    id: 'swOyWKk7Oko',
    dep: `Nằm ngửa tay để sau đầu
Sau đó nhấc trên khỏi sàn rồi từ từ trở về tư thế ngồi.
Từ từ quay trở lại tư thế bắt đầu và lặp lại bài tập này.`,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Nâng chân',
    id: 'dGKbTKLnym4',
    dep: `Nằm ngửa tay đỡ dưới hông
Sau đó nâng chân lên cho đến khi tạo thành một góc thẳng với sàn.
Tù tù hạ chân xuống rồi lặp lại bài tập này.`,
    type: 1,
    numCalo: 1.3,
  },
  {
    title: 'Đo sàn',
    id: 'Fcbw82ykBvY',
    dep: `Nằm trên sàn, chạm sàn bằng ngón chân và cẳng tay. Giữ thẳng cơ thể và giữ tư thế càng lâu càng tốt.
Bài tập này làm khoẻ cơ bụng, lưng và vai.`,
    type: 2,
    numCalo: 1.4,
  },
  {
    title: 'Leo núi',
    id: 'wQq3ybaLZeA',
    dep: `2 bàn tay chạm xuống sàn nhà toàn bộ thân người từ chân, hông, lưng, cổ, đầu tạo thành một
Thực hiện động tác kéo chân trái về phía tay trái, sau đó đưa chân trái về vị trí bắt đầu và đưa chân phải lên phía tay phải, sau đó đưa chân phải về vị trí bắt đầu
Đó là 1 lần lặp và thực hiện liên tục các động tác như trên để tiếp tục bài tập, thực hiện càng nhanh càng tốt`,
    type: 2,
    numCalo: 1.5,
  },
  {
    title: 'Gập cánh tay dài',
    id: 'GxKoSEkmRC8',
    dep: `Nằm ngửa, đầu gối cong, duỗi cánh tay của bạn thẳng ra phía sau.
Di chuyển cánh tay thẳng lên trên đầu, từ từ nâng cả đầu và cánh tay gập người lên nhưng không quá cao, rồi hạ người về vị trí cũ.
    `,
    type: 2,
    numCalo: 1.4,
  },
  {
    title: 'Xoay người',
    id: 'DJQGX2J4IVw',
    dep: `2 đầu gối hơi gập, 2 chân giơ hơi cao lên để thân người và chân tạo thành hình chữ V, mũi chân hướng lên cao, gót chân hướng xuống dưới đất.
Giữ nguyên mông trên sàn. Vặn thân người và 2 tay sang trái, đồng thời hơi vặn 2 đầu gối sang phải.
Lặp lại động tác tương tự với bên phải. Rồi quay trở lại vị trí ban đầu. Lưu ý, trong toàn bộ các động tác tập luyện, bạn cần giữ nguyên vị trí 2 tay và căng cứng cơ bụng`,
    type: 2,
    numCalo: 1.3,
  },
  {
    title: 'Plank nghiêng',
    id: '2W96p2PIoPg',
    dep: `Nằm nghiêng người sang bên trái, 2 chân duỗi thẳng, xếp chồng chân phải lên chân trái.
    - Cẳng tay trái đặt dưới vai, chống vuông góc lên sàn.
    - Tay phải chống lên hông hoặc giơ cao thẳng lên trần nhà.
    - Toàn bộ thân người giữ cố định, tạo thành 1 đường thẳng.`,
    type: 2,
    numCalo: 1.5,
  },
  {
    title: 'Đạp xe trên không',
    id: '-nJkAJpQemI',
    dep: `Nằm nghiêng người sang bên trái, 2 chân duỗi thẳng, xếp chồng chân phải lên chân trái.
    - Cẳng tay trái đặt dưới vai, chống vuông góc lên sàn.
    - Tay phải chống lên hông hoặc giơ cao thẳng lên trần nhà.
    - Toàn bộ thân người giữ cố định, tạo thành 1 đường thẳng.`,
    type: 2,
    numCalo: 1.4,
  },

];
