export const dataTapChan = [
  {
    title: 'Leo núi',
    id: 'K0R4snqCbRA',
    dep: `Bắt đầu ở tư thế chống đẩy. Co gối về phía ngực và giữ chân trái thẳng, sau đó nhanh chóng chuyển sang chân khác.
Bài tập này tăng cường nhiều nhóm cơ.`,
    type: 1,
    numCalo: 1.5,
  },
  {
    title: 'Xoay đầu gối',
    id: 'cYDkPDqeY5E',
    dep: `Đứng thẳng. Đặt hai tay vào hai đầu gối, xoay khớp đầu gối theo chiều kim đồng hồ.
Xoay 4 vòng thì đổi chiều`,
    type: 1,
    numCalo: 1.3,
  },
  {
    title: 'Đứng tấn',
    id: 'XEKiRnwBfYA',
    dep: `Đứng để tay lên hông và chân đứng hơi rộng hơn vai.Sau đó hạ cơ thể xuống cho đến khi đùi song song với sàn.
Đầu gối mở ra cùng hướng với ngón chân. Lặp lại từ 10-15 lần cho bài tập này.`,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Nằm nghiêng nâng chân',
    id: 'VlwBJE1WtOQ',
    dep: `Nằm nghiêng đầu tựa vào tay phải. Nhấc chân trên rồi trở lại tư thế ban đầu.
Lời khuyên: Đảm bảo chân trái phải đưa thẳng lên và xuống trong khi tập.`,
    type: 1,
    numCalo: 1.4,
  },
];
