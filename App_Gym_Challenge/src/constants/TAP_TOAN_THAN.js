export const dataTapToanThan = [
  {
    title: 'Tập cơ bụng',
    id: 'RUNrHkbP4Pc',
    dep: `Nằm ngửa cong gối và tay duỗi thẳng. Sau đó nhấc thân trên khỏi sàn. Giữ một vài giây rồi từ từ trở lại tư thế ban đầu.
Bài tập này chủ yếu cho các cơ bụng thẳng và cơ liên sườn
        `,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Đứng tấn',
    id: 'XEKiRnwBfYA',
    dep: `Đứng để tay lên hông và chân đứng hơi rộng hơn vai.Sau đó hạ cơ thể xuống cho đến khi đùi song song với sàn.
Đầu gối mở ra cùng hướng với ngón chân. Lặp lại từ 10-15 lần cho bài tập này.`,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Gánh đùi',
    id: '42bFodPahBU',
    dep: `Chân đứng rộng bằng vai, tay để thẳng về trước, hạ thấp cơ thể cho tới khi đùi song song với sàn.
Đầu gối cùng hướng với ngón chân. Quay lại tư thế ban đầu và lặp lại.
Bài tập này giúp tập đùi, hông, cơ đùi trước, gân kheo và thân dưới.`,
    type: 1,
    numCalo: 1.4,
  },
  {
    title: 'Chống đẩy',
    id: 'R08gYyypGto',
    dep: `Nằm áp xuống sàn lấy tay đỡ thân. Giữ thẳng thân trong khi nâng và hạ thân bằng tay.
Bài tập này giúp tập ngực, vai, cơ tay sau, lưng và chân.`,
    type: 1,
    numCalo: 1.4,
  },
];
